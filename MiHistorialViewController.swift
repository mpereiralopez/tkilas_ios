//
//  MiHistorialViewController.swift
//  Tkilas
//
//  Created by Miguel Pereira Lopez on 03/10/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit
import CoreData


class MiHistorialViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var offerList: UITableView!
    
    var appDel:AppDelegate!;
    var context:NSManagedObjectContext!;
    var en:NSEntityDescription!;
    var offerListManaged: [NSManagedObject]!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.appDel = UIApplication.sharedApplication().delegate as AppDelegate;
        self.context = appDel.managedObjectContext!
        self.en = NSEntityDescription.entityForName("Local", inManagedObjectContext: context);
        
        let entName:String! = en?.name;
        let freq = NSFetchRequest(entityName: entName)
        self.offerListManaged  = context.executeFetchRequest(freq, error: nil) as? [NSManagedObject];
        
        self.offerList.separatorColor = ViewControllerUtils().UIColorFromHex(0x8CD200, alpha: 1);
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
    }

    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        println("\(self.offerListManaged.count)");
        return self.offerListManaged.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("OfferCell") as? OfferListCellTableViewCell
        
        if !(cell != nil) {
            cell = OfferListCellTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "OfferCell")

        }
        
        var title = offerListManaged[indexPath.row].valueForKey("offer") as String;
        cell?.offerDetail.text = title;
        
        var localName = offerListManaged[indexPath.row].valueForKey("local_name") as String
        
        var hour = offerListManaged[indexPath.row].valueForKey("hour") as String;
        
        var dateFormatter = NSDateFormatter();
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        var date = offerListManaged[indexPath.row].valueForKey("date") as NSDate;

        
        var textDate = dateFormatter.stringFromDate(date);

        
        var textToDate = "\(localName), \(textDate) a las \(hour).";
        
        cell?.offerDate.text = textToDate;

                
        return cell!
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let owc = storyBoard.instantiateViewControllerWithIdentifier("offerView") as OfferSelectedViewController;

        var offerIndex = indexPath.row;
        
        let appDel2 = UIApplication.sharedApplication().delegate as AppDelegate;
        let context2 = appDel2.managedObjectContext!
        let en2 = NSEntityDescription.entityForName("User", inManagedObjectContext: context2);
        let entName:String! = en2?.name;
        let freq = NSFetchRequest(entityName: entName)
        let userList: [NSManagedObject]?  = context2.executeFetchRequest(freq, error: nil) as? [NSManagedObject]
        println("Si hay usuario")
        let firstContact = userList![0]
        var objAux = self.offerListManaged[offerIndex];
        var attr = objAux.entity.attributesByName as NSDictionary
        var info = objAux.dictionaryWithValuesForKeys(attr.allKeys)
        objAux.setValuesForKeysWithDictionary(info);
        owc.jsonResult = info;
        owc.ownerName = firstContact.valueForKey("userName") as String;
        owc.offerDetailFromConfirmation = offerListManaged[offerIndex].valueForKey("offer") as String;
        owc.localSelectedImgURL = offerListManaged[offerIndex].valueForKey("pic_url") as String;
        owc.localName = offerListManaged[offerIndex].valueForKey("local_name") as String;
        self.presentViewController(owc, animated: true, completion: nil)

    }

}
