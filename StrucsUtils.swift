//
//  StrucsUtils.swift
//  Tkilas
//
//  Created by Miguel on 18/09/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import Foundation


/*


*/

class Pack{
    var date: String;
    var time_ini:String;
    var time_fin:String;
    var discount:Int;
    
    var promo_type1:Int;
    var promo_subtype1:Int;
    var promo_size1:Int;
    var promo_pvp1:Double;
    var promo_max_person1:Int;
    
    var promo_type2:Int;
    var promo_subtype2:Int;
    var promo_size2:Int;
    var promo_pvp2:Double;
    var promo_max_person2:Int;
    
    init(date:String, time_ini:String, time_fin:String, promo_type1:Int, discount:Int, promo_subtype1:Int, promo_size1:Int, promo_pvp1:Double, promo_max_person1:Int
        , promo_type2:Int, promo_subtype2:Int, promo_size2:Int, promo_pvp2:Double, promo_max_person2:Int){
    
            self.date = date;
            self.time_fin = time_fin;
            self.time_ini = time_ini;
            self.discount = discount;
            self.promo_type1 = promo_type1;
            self.promo_type2 = promo_type2;
            self.promo_subtype1 = promo_subtype1;
            self.promo_subtype2 = promo_subtype2;
            self.promo_size1 = promo_size1;
            self.promo_size2 = promo_size2;
            self.promo_pvp1 = promo_pvp1;
            self.promo_pvp2 = promo_pvp2;
            self.promo_max_person1 = promo_max_person1;
            self.promo_max_person2 = promo_max_person2;
    }
    
    }

class Local {
    var userIdUser: Int{
        get{
            return self.userIdUser;
        }
        set (newValue){
            self.userIdUser = newValue;

        }
    }
    var local_name: String{
        get{
            return self.local_name;
        }
        set (newValue){
            self.local_name = newValue;
            
        }
    }

    var tlf: String{
        get{
            return self.tlf;
        }
        set (newValue){
            self.tlf = newValue;
            
        }
    }

    var address: String{
        get{
            return self.address;
        }
        set (newValue){
            self.address = newValue;
            
        }
    }

    var local_cp: String{
        get{
            return self.local_cp;
        }
        set (newValue){
            self.local_cp = newValue;
            
        }
    }

    var city: String{
        get{
            return self.city;
        }
        set (newValue){
            self.city = newValue;
            
        }
    }

    var web: NSURL{
        get{
            return self.web;
        }
        set (newValue){
            self.web = newValue;
            
        }
    }

    var capacity: Int{
        get{
            return self.capacity;
        }
        set (newValue){
            self.capacity = newValue;
            
        }
    }

    var local_type:Int{
        get{
            return self.local_type;
        }
        set (newValue){
            self.local_type = newValue;
            
        }
    }

    var product_type_local: Int{
        get{
            return self.product_type_local;
        }
        set (newValue){
            self.product_type_local = newValue;
            
        }
    }

    var local_latitud: Double{
        get{
            return self.local_latitud;
        }
        set (newValue){
            self.local_latitud = newValue;
            
        }
    }

    var local_longitud: Double{
        get{
            return self.local_longitud;
        }
        set (newValue){
            self.local_longitud = newValue;
            
        }
    }

    var url_pic1: NSURL{
        get{
            return self.url_pic1;
        }
        set (newValue){
            self.url_pic1 = newValue;
            
        }
    }

    var url_pic2: NSURL{
        get{
            return self.url_pic2;
        }
        set (newValue){
            self.url_pic2 = newValue;
            
        }
    }

    var url_pic3: NSURL{
        get{
            return self.url_pic3;
        }
        set (newValue){
            self.url_pic3 = newValue;
            
        }
    }

    var local_desc: String{
        get{
            return self.local_desc;
        }
        set (newValue){
            self.local_desc = newValue;
            
        }
    }

    var local_pay_way: Int{
        get{
            return self.local_pay_way;
        }
        set (newValue){
            self.local_pay_way = newValue;
            
        }
    }

    var local_cost_beer: Double{
        get{
            return self.local_cost_beer;
        }
        set (newValue){
            self.local_cost_beer = newValue;
            
        }
    }

    var local_cost_cocktail: Double{
        get{
            return self.local_cost_cocktail;
        }
        set (newValue){
            self.local_cost_cocktail = newValue;
            
        }
    }

    var local_cost_alch_bottle: Double{
        get{
            return self.local_cost_alch_bottle;
        }
        set (newValue){
            self.local_cost_alch_bottle = newValue;
            
        }
    }

    var local_cost_wine_bottle: Double{
        get{
            return self.local_cost_wine_bottle;
        }
        set (newValue){
            self.local_cost_wine_bottle = newValue;
            
        }
    }

    var local_cost_coffe: Double{
        get{
            return self.local_cost_coffe;
        }
        set (newValue){
            self.local_cost_coffe = newValue;
            
        }
    }

    var local_cost_tonic: Double{
        get{
            return self.local_cost_tonic;
        }
        set (newValue){
            self.local_cost_tonic = newValue;
            
        }
    }

    //var packs: [],
    //var comments: []

    init (userIdUser: Int, local_name:String, tlf:String, address:String, local_cp:String, city:String, web:NSURL, capacity:Int, local_type:Int, product_type_local:Int, local_latitud:Double,
        local_longitud:Double,url_pic1:NSURL,url_pic2:NSURL,url_pic3:NSURL,local_desc:String, local_pay_way:Int, local_cost_beer:Double, local_cost_cocktail:Double,local_cost_alch_bottle:Double,
        local_cost_wine_bottle:Double,local_cost_coffe:Double,local_cost_tonic:Double){
            self.userIdUser = userIdUser;
            self.local_name = local_name;
            self.tlf = tlf;
            self.address = address;
            self.local_cp = local_cp;
            self.city = city;
            self.web = web;
            self.capacity = capacity;
            self.local_type = local_type;
            self.product_type_local = product_type_local;
            self.local_latitud = local_latitud;
            self.local_longitud = local_longitud;
            self.url_pic1 = url_pic1;
            self.url_pic2 = url_pic2;
            self.url_pic3 = url_pic3;
            self.local_desc = local_desc;
            self.local_pay_way = local_pay_way;
            self.local_cost_beer = local_cost_beer;
            self.local_cost_cocktail = local_cost_cocktail;
            self.local_cost_alch_bottle = local_cost_alch_bottle;
            self.local_cost_wine_bottle = local_cost_wine_bottle;
            self.local_cost_coffe = local_cost_coffe;
            self.local_cost_tonic  = local_cost_tonic;
    }
}


struct SomeStruct2 {
    var name: String
    init(name: String) {
        self.name = name
    }
}