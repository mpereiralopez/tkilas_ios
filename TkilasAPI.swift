//
//  TkilasAPI.swift
//  Tkilas
//
//  Created by Miguel on 22/09/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit


protocol TkilasAPIProtocol{
    func didRecieveResponse(results: NSArray)
}

class TkilasAPI: NSObject {
    
    var delegate: TkilasAPIProtocol?
    
    let API_KEY = "NkRulvqxJ33bTvnz6FwHjGZoQlfMnuVU";

    
    func getLocalListAsync(urlPath:String, username:String, password:String){
        
        let url: NSURL = NSURL(string: urlPath)
        let session = NSURLSession.sharedSession()
        
        let userPasswordString = username+":"+password
        let userPasswordData = userPasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = userPasswordData!.base64EncodedStringWithOptions(nil)
        let authString = "Basic \(base64EncodedCredential)"
        
        session.configuration.HTTPAdditionalHeaders = ["API_KEY":API_KEY, "Authorization":authString];
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            
            if error != nil {
                // If there is an error in the web request, print it to the console
                println(error.localizedDescription)
            }
            
            var err: NSError?
            var jsonResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as NSArray
            if err != nil {
                // If there is an error parsing JSON, print it to the console
                println("JSON Error \(err!.localizedDescription)")
            }
            
            self.delegate?.didRecieveResponse(jsonResult);

           
        })
        task.resume()

        
    }
}
