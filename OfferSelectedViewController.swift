//
//  OfferSelectedViewController.swift
//  Tkilas
//
//  Created by Miguel Pereira Lopez on 03/10/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit

class OfferSelectedViewController: UIViewController {
    
    
    @IBOutlet weak var navigationBar: UINavigationItem!
    
    @IBOutlet weak var localImage: UIImageView!
    @IBOutlet weak var offerDetail: UIButton!
    @IBOutlet weak var offerOwner: UILabel!
    @IBOutlet weak var offerDate: UILabel!
    @IBOutlet weak var offerHour: UILabel!
    @IBOutlet weak var offerSize: UILabel!

    
    var jsonResult:NSDictionary!;
    var ownerName:String!;
    var offerDetailFromConfirmation:String!;
    var localSelectedImgURL:String!;
    var localName:String!;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.offerOwner.text = "Oferta a nombre de \(self.ownerName)";
        self.offerDetail.titleLabel?.text = self.offerDetailFromConfirmation;
        self.offerDetail.setTitle(self.offerDetailFromConfirmation, forState: UIControlState.Normal);
        self.navigationBar.title = self.localName;
        let size = self.jsonResult.valueForKey("size") as Int;
        self.offerSize.text = "\(size) personas";
        
        self.offerHour.text = self.jsonResult.valueForKey("hour") as? String;
        
        var dateFormatter = NSDateFormatter();
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        var date = self.jsonResult.valueForKey("date") as NSDate;
        
        var textDate = dateFormatter.stringFromDate(date);
        
        
        self.offerDate.text = textDate;
        // Do any additional setup after loading the view.
        
        if (localSelectedImgURL != nil){
            var urlString = localSelectedImgURL;
            urlString = API_LOCATION().API_PICS_HOST+urlString;
            println(urlString)
            var url:NSURL = NSURL(string: urlString)!
            var data:NSData = NSData(contentsOfURL: url, options: nil, error: nil)!
            localImage.image = UIImage(data: data)
        }else{
            localImage.image = UIImage(named:"local_NOPIC.png")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
