//
//  LocalListCellTableViewCell.swift
//  Tkilas
//
//  Created by Miguel on 23/09/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit

class LocalListCellTableViewCell: UITableViewCell {

    @IBOutlet var localAddressLabel: UILabel!
    @IBOutlet var localNameLabel: UILabel!
    @IBOutlet var localImg: UIImageView!
    @IBOutlet var localDistance: UILabel!

    @IBOutlet var noOffer: UILabel!

    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
