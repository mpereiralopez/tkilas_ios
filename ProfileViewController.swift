//
//  ProfileViewController.swift
//  Tkilas
//
//  Created by Miguel Pereira Lopez on 29/09/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit
import CoreData

class ProfileViewController: UIViewController,  UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!

    @IBOutlet weak var emailTextField: UITextField!
    
    var appDel:AppDelegate!;
    var context:NSManagedObjectContext!;
    var en:NSEntityDescription!;
    var firstContact: NSManagedObject!
    var user: Model_User!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.appDel = UIApplication.sharedApplication().delegate as AppDelegate;
        self.context = appDel.managedObjectContext!
        self.en = NSEntityDescription.entityForName("User", inManagedObjectContext: context);
        
        let entName:String! = en?.name;
        let freq = NSFetchRequest(entityName: entName)
        
        let userList: [NSManagedObject]?  = context.executeFetchRequest(freq, error: nil) as? [NSManagedObject]

        println("Si hay usuario")
        self.firstContact = userList![0]
        
        self.user = Model_User(entity: en!, insertIntoManagedObjectContext: context)

        
        nameTextField.text = firstContact.valueForKey("userName") as String
        emailTextField.text = firstContact.valueForKey("userEmail") as String;
        let imgData = firstContact.valueForKey("user_pic") as NSData!
        if imgData != nil {
            self.profilePic.image = UIImage(data: firstContact.valueForKey("user_pic") as NSData);
        }
        
      
        //AQUI GESTIONO TODO LO DEL USUARIO
        self.profilePic.layer.borderWidth = 2;
        self.profilePic.layer.borderColor = ViewControllerUtils().UIColorFromHex(0x999999, alpha: 1).CGColor;
        self.profilePic.backgroundColor = ViewControllerUtils().UIColorFromHex(0x00FF00, alpha: 1)
        
        self.profilePic.userInteractionEnabled = true;
        var gestureTap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("oneTap:"));
        self.profilePic.addGestureRecognizer(gestureTap)
            
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func oneTap(gesture: UITapGestureRecognizer) {
        var picker: UIImagePickerController = UIImagePickerController();
        picker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
        picker.delegate = self;
        picker.allowsEditing = false;
        self.presentViewController(picker, animated: true, completion: nil);
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation*/
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        println (segue.identifier)
        // pass data to next view
        let segueIdentifier:String = segue.identifier!;
        var viewControllerDateSelector:MainWindowController = segue.destinationViewController as MainWindowController;

        //if(back_to_main_view )
    }
    
    
    //UIImagePickerControllerDelegate Methods
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]){
        
        let imageFromGallery = info[UIImagePickerControllerOriginalImage] as UIImage;
        //LA ENVIO AL WS
        /*let appDel:AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate;
        let context:NSManagedObjectContext = appDel.managedObjectContext!;
        
        var fetchedResultController: NSFetchedResultsController = NSFetchedResultsController()


        
        let entityDescripition = NSEntityDescription.entityForName("User", inManagedObjectContext: context);
        
        let user = Model_User(entity: entityDescripition!, insertIntoManagedObjectContext: context)*/

        
        var dataImageToSave: NSData? = UIImageJPEGRepresentation( imageFromGallery, 1) as NSData;
        self.firstContact.setValue(dataImageToSave, forKey: "user_pic")
        
        
        context.save(nil)
        println("User saved");
        //
        self.profilePic.image = imageFromGallery;
        picker.dismissViewControllerAnimated(true, completion: nil)

    
    }
    func imagePickerControllerDidCancel(picker: UIImagePickerController){
        picker.dismissViewControllerAnimated(true, completion: nil)
    
    }

}
