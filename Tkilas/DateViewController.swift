//
//  ViewController2.swift
//  Tkilas
//
//  Created by Miguel Pereira Lopez on 13/08/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit


class DateViewController: UIViewController, CLLocationManagerDelegate,UISearchBarDelegate, UISearchDisplayDelegate{

    var productSelected: Int!
    var dateSelected: NSDate!
    let locationManager = CLLocationManager();
    var isNotDateChecked: Bool = false;
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var numOfPerson: UITextField!
    @IBOutlet weak var buttonChangeDate: UIButton!
    @IBOutlet weak var personPlus: UIButton!
    @IBOutlet weak var personMinus: UIButton!
    @IBOutlet weak var whenAsk: UILabel!
    
    @IBOutlet weak var buttonChangeHour: UIButton!
    @IBOutlet weak var nowDateBtn: UIButton!
    @IBOutlet weak var dateBtn: UIButton!
    @IBOutlet weak var hourBtn: UIButton!
    @IBOutlet weak var datePickerSelector: UIDatePicker!
    @IBOutlet weak var searchButton: UIButton!
    
    var customView:UIView!;
    
    @IBOutlet weak var labelTrick2: UILabel!
    @IBOutlet weak var labelTrick1: UILabel!
    var results: MKLocalSearchResponse! ;
    
    
    var location: CLLocationCoordinate2D!;
    
    
    //BOTONES DE FILTRO
    
    
    /////////////////////
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchBar.delegate = self;
        self.searchDisplayController?.delegate = self;
        self.numOfPerson.text = "2";
        self.personMinus.layer.borderWidth = 1.0;
        self.personPlus.layer.borderWidth = 1.0;

        self.datePickerSelector.timeZone = NSTimeZone.systemTimeZone();
        
        let currentDate = NSDate();
        
        
        self.datePickerSelector.hidden = true;
        self.datePickerSelector.backgroundColor  = UIColor.whiteColor();
        self.datePickerSelector.minimumDate = currentDate;
        self.datePickerSelector.date = currentDate
        
        println(self.datePickerSelector.date);
        self.datePickerSelector.addTarget(self, action: Selector("datePickerChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        
        var color : UIColor = self.UIColorFromHex(0x999999, alpha: 1);
        self.personMinus.layer.borderColor = color.CGColor
        self.personPlus.layer.borderColor = color.CGColor
        //self.tableView.reloadData();
        self.tableView.hidden = true;
        self.searchBar.layer.borderWidth = 1.0;
        self.searchBar.layer.borderColor = color.CGColor;
        self.getCurrentTime();

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func getCurrentGPSPosition(sender: AnyObject) {
        println("Pulsaste para localización");
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestAlwaysAuthorization();
        self.locationManager.requestWhenInUseAuthorization();
        self.locationManager.startUpdatingLocation();
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        let segueIdentifier:String = segue.identifier!;
        println(segueIdentifier);
        if(segueIdentifier == "searchLocalSegue"){
            var viewMapSelector:MapViewController = segue.destinationViewController as MapViewController;
            viewMapSelector.productSelected = self.productSelected;
            viewMapSelector.dateSelected = self.dateSelected;
            println(self.buttonChangeHour.titleLabel?.text);
            viewMapSelector.counter = self.numOfPerson.text.toInt();
            viewMapSelector.latitud = String(format:"%f", self.location.latitude);
            viewMapSelector.longitud = String(format:"%f", self.location.longitude);
            
            
        }else if(segueIdentifier == "backHomeSiegue"){
            var homeView:MainWindowController = segue.destinationViewController as MainWindowController;

        }

    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: {(placemarks, error)->Void in
            println("---------------------");
            if (error != nil) {
                println("Reverse geocoder failed with error" + error.localizedDescription)
                return
            }
            
            if placemarks.count > 0 {
                let pm = placemarks[0] as CLPlacemark;
                self.displayLocationInfo(pm)
            } else {
                println("Problem with the data received from geocoder")
            }
        })
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println("Error while updating location " + error.localizedDescription)

    }
    
    
    func displayLocationInfo(placemark: CLPlacemark?) {
        println("Aquí");

        if let containsPlacemark = placemark {
            //stop updating location to save battery life
            self.locationManager.stopUpdatingLocation();
            //let locality = (containsPlacemark.locality != nil) ? containsPlacemark.locality : "";
            //let postalCode = (containsPlacemark.postalCode != nil) ? containsPlacemark.postalCode : "";
            //let administrativeArea = (containsPlacemark.administrativeArea != nil) ? containsPlacemark.administrativeArea : "";
            //let country = (containsPlacemark.country != nil) ? containsPlacemark.country : "";
            println(containsPlacemark.description);
            self.searchBar.placeholder = containsPlacemark.name;
            self.location = placemark?.location.coordinate;
            
        }
    }
    
    /***** FUNCIONES DE FECHAS ***********/
    
    
    
    func datePickerChanged (sender: UIDatePicker){
        
        let date = sender.date;
        self.dateSelected = date;
        let calendar = NSCalendar.currentCalendar();
        let components = calendar.components( NSCalendarUnit.CalendarUnitMonth | NSCalendarUnit.CalendarUnitDay | NSCalendarUnit.CalendarUnitHour | NSCalendarUnit.CalendarUnitMinute, fromDate: date)
        var hourForDisplay:String!;
        var minuteForDisplay:String!;
        
        if(components.hour < 10){
            hourForDisplay = "0"+String(components.hour)
        }else{
            hourForDisplay = String(components.hour);
        }
        
        if(components.minute < 10){
            minuteForDisplay = "0"+String(components.minute);
        }else{
            minuteForDisplay = String(components.minute);
        }
        self.buttonChangeHour.setTitle("\(hourForDisplay):\(minuteForDisplay)", forState: UIControlState.Normal)
        self.buttonChangeDate.setTitle("\(components.day)/\(components.month)", forState: UIControlState.Normal)
        self.anyDateButtonPressed();
        
    }
    
    
    
    @IBAction func nowButtonPressed(sender: AnyObject) {
        self.getCurrentTime();
        
        //NSDate(dateString:"2014-06-06")2014-10-02 19:00:00 +0000
    }
    
    func getCurrentTime(){
        let today:NSDate = NSDate();
        self.dateSelected = today;
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.CalendarUnitHour | .CalendarUnitMinute | .DayCalendarUnit | .MonthCalendarUnit , fromDate: today)
        var hour = components.hour
        var minutes = components.minute
        println(components.description);
        var minuteText:String = String();
        if(minutes<=30){
            minuteText = "30"
        }else{
            if(minutes>30 && minutes < 60){
                minuteText = "00";
                hour = hour + 1;
            }
        }
        
        self.buttonChangeHour.setTitle("\(hour):\(minuteText)", forState: UIControlState.Normal)
        self.buttonChangeDate.setTitle("\(components.day)/\(components.month)", forState: UIControlState.Normal)
    }
    
    @IBAction func buttonChangeDatePressed(sender: AnyObject) {
         self.anyDateButtonPressed();
    }
    

    
    @IBAction func buttonChangeHourPressed(sender: AnyObject) {
        self.anyDateButtonPressed();
    }
    
    
    func anyDateButtonPressed(){
        if(self.datePickerSelector.hidden){
            self.datePickerSelector.hidden = false;
            self.searchButton.enabled = false;
            self.searchButton.alpha = 0.6;
            self.labelTrick1.hidden = true;
            self.labelTrick2.hidden = true;
            
            
        }else{
            self.datePickerSelector.hidden = true;
            self.searchButton.enabled = true;
            self.searchButton.alpha = 1.0;
            self.labelTrick1.hidden = false;
            self.labelTrick2.hidden = false;
        }

    }
    
    
    
    
    @IBAction func noDateRadio(sender: AnyObject) {
        
        println(sender.currentBackgroundImage);
        
        if(self.isNotDateChecked){
            var image: UIImage = UIImage(named: "radioNO.png")!
            sender.setBackgroundImage(image, forState: UIControlState.Normal);
            self.isNotDateChecked = false;
            self.dateBtn.enabled = true;
            self.hourBtn.enabled = true;
            self.nowDateBtn.enabled = true;
            self.nowDateBtn.alpha = 1.0;
            self.whenAsk.alpha = 1.0;

        }else{
            var image: UIImage = UIImage(named: "radioSI.png")!
            sender.setBackgroundImage(image, forState: UIControlState.Normal);
            self.isNotDateChecked = true;
            //CAMBIO COSAS
            self.dateBtn.enabled = false;
            self.hourBtn.enabled = false;
            self.nowDateBtn.enabled = false;
            self.nowDateBtn.alpha = 0.6;
            self.whenAsk.alpha = 0.6;

        }
        
        self.dateSelected = NSDate();
    }
    
    /********************************************************/
    @IBAction func personPlus(sender: AnyObject) {
        var actualValue:Int? = self.numOfPerson.text.toInt();
        self.numOfPerson.text = "\(actualValue! + 1)";
        
    }
    
    @IBAction func personMinus(sender: AnyObject) {
        var actualValue:Int? = self.numOfPerson.text.toInt();
        if((actualValue! - 1) > 0){
            self.numOfPerson.text = "\(actualValue! - 1)";
        }

    }
    
    /********************************************************/


    func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar){
        self.searchDisplayController?.searchResultsTableView.reloadData();
        self.searchDisplayController?.searchResultsTableView.hidden = true;
    }

    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        self.searchDisplayController?.searchResultsTableView.reloadData();
        self.searchDisplayController?.searchResultsTableView.hidden = false;
    }

    
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar){
        self.searchDisplayController!.active = true;
        self.searchDisplayController?.searchResultsTableView.reloadData();
    }
    
    func searchBar( searchBar: UISearchBar, textDidChange searchText: String){
        println("Me estas cambiando");
        self.searchDisplayController?.searchResultsTableView.reloadData();

        //self.searchBar.resignFirstResponder();
        //self.searchDisplayController!.active = true;
        //self.searchDisplayController!.searchResultsTableView.reloadData();
        
    }
    
    
    func searchDisplayController(controller: UISearchDisplayController, shouldReloadTableForSearchString searchString: String!) -> Bool {   // create search request
        var searchRequest = MKLocalSearchRequest();
        searchRequest.naturalLanguageQuery = searchString;

        var localSearch = MKLocalSearch (request: searchRequest);
        
        localSearch.startWithCompletionHandler {
            (response: MKLocalSearchResponse! , error: NSError! )  in
            self.results = response;
            self.searchDisplayController?.searchResultsTableView.reloadData();
            if error == nil {
                var placemarks:NSMutableArray = NSMutableArray()
                for item in response.mapItems {
                    placemarks.addObject(placemarks)
                }
               
            } else {
                println("Fallo aqui");
                println(error);
            }
        }

    
    return true;
    }
    
    /*func searchDisplayControllerDidBeginSearch(controller: UISearchDisplayController){
        self.results.
    }*/

    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.searchDisplayController!.searchResultsTableView {
            if(self.results != nil){
                println(self.results.mapItems.count);
                return self.results.mapItems.count
            }else{
                println("AQUI 1");
                return 0;
            }
            
        } else {
            println("AQUI 2");
            return 0;
        }    }
    

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("Cell") as? UITableViewCell
        
        if !(cell != nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        }
        
        dispatch_async(dispatch_get_main_queue()) {
        
        

        if tableView == self.searchDisplayController!.searchResultsTableView {
            self.results.mapItems[indexPath.row]
            cell!.textLabel.text = self.results.mapItems[indexPath.row].name
        } else {
        }
        // Configure the cell
        cell!.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.searchDisplayController?.setActive(false, animated: true);
        //self.searchDisplayController?.searchResultsTableView.setEditing(false, animated: false);
        var place = results.mapItems[indexPath.row] as MKMapItem;
        self.searchBar.text = place.placemark.description;
        self.location = CLLocationCoordinate2D(latitude: place.placemark.location.coordinate.latitude, longitude: place.placemark.location.coordinate.longitude);
        self.searchBar.endEditing(true);
        self.searchBar.resignFirstResponder();
        self.searchDisplayController?.searchResultsTableView.reloadData();
    }
}
