//
//  ConfirmViewController.swift
//  Tkilas
//
//  Created by Miguel Pereira López on 16/11/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit
import CoreData
import MapKit

class ConfirmViewController: UIViewController, TkilasAPIReservationProtocol {
    
    
    let viewContainerForSpinner: UIView = UIView();
    var spinner: UIActivityIndicatorView!;
    
    var api: TkilasAPI = TkilasAPI();
    var localLongitud : CLLocationDegrees = 0.0;
    var localLatitud : CLLocationDegrees = 0.0;
    var counter: Int!;
    var productSelected: Int!;
    var dateSelected: NSDate!;
    var tableData: NSArray = NSArray();
    var userLat:String!;
    var userLon:String!;
    var pack: Array<AnyObject> = [];
    var localSelected:NSDictionary?;
    var buttonData:NSDictionary!;
    
    var hourForWS:String!;
    
    var hourSelected:String!;
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeIni: UIButton!
    @IBOutlet weak var timePlus30: UIButton!
    @IBOutlet weak var timePlus60: UIButton!
    @IBOutlet weak var num_persons: UILabel!

    @IBOutlet weak var offerSelectedInfo: UILabel!
    
    
    var ownerName:String!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        api.delegateReservation = self;

        self.num_persons.text = "\(self.counter) personas";
        var utils = ViewControllerUtils();

        
        var dateFormatter = NSDateFormatter();
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        
        self.dateLabel.text = dateFormatter.stringFromDate(self.dateSelected);
        
        
        
        var textToShow:String = self.buttonData.valueForKey("textToShow") as String;
        self.offerSelectedInfo.text = textToShow;
        
        
        var rangeOfdoublePoint = hourSelected.rangeOfString(":", options: nil, range: nil, locale: nil)?.startIndex;
        let hourText = hourSelected.substringWithRange(Range<String.Index>(start: hourSelected.startIndex, end: rangeOfdoublePoint!))
        let minuteText = hourSelected.substringWithRange(Range<String.Index>(start: advance(rangeOfdoublePoint!, 1), end: hourSelected.endIndex))
        var hour = hourText.toInt()!;
        var minutes = minuteText.toInt()!;

        if(minutes == 0){
            self.timeIni.titleLabel?.text = "\(hour):00";
            self.timeIni.setTitle("\(hour):00", forState: UIControlState.Normal)
            self.timePlus30.titleLabel?.text = "\(hour):30";
            self.timePlus30.setTitle("\(hour):30", forState: UIControlState.Normal)
            
            if(hour+1 == 24){
                self.timePlus60.titleLabel?.text = "00:00";
                self.timePlus60.setTitle("00:00", forState: UIControlState.Normal)
            }else{
                self.timePlus60.titleLabel?.text = "\(hour+1):00";
                self.timePlus60.setTitle("\(hour+1):00", forState: UIControlState.Normal)
            }
            
            


        }else{
            self.timeIni.titleLabel?.text = "\(hour):30";
            self.timeIni.setTitle("\(hour):30", forState: UIControlState.Normal)

            if(hour+1 == 24){
                self.timePlus30.titleLabel?.text = "00:00";
                self.timePlus30.setTitle("00:00", forState: UIControlState.Normal)
                
                self.timePlus60.titleLabel?.text = "00:30";
                self.timePlus60.setTitle("00:30", forState: UIControlState.Normal)
            }else{
                self.timePlus30.titleLabel?.text = "\(hour+1):00";
                self.timePlus30.setTitle("\(hour+1):00", forState: UIControlState.Normal)
                
                self.timePlus60.titleLabel?.text = "\(hour+1):30";
                self.timePlus60.setTitle("\(hour+1):30", forState: UIControlState.Normal)
            }
            
            

        }
        
        self.hourForWS = self.timeIni.titleLabel?.text;
        println("Hora para WS: \(self.hourForWS)")

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func setHourToIni(sender: AnyObject) {
        self.hourForWS = self.timeIni.titleLabel?.text;
        println("Hora para WS: \(self.hourForWS)");
        
        self.timeIni.backgroundColor = self.UIColorFromRGB(0x999999);
        self.timePlus30.backgroundColor = self.UIColorFromRGB(0x00FF00);
        self.timePlus60.backgroundColor = self.UIColorFromRGB(0x00FF00);

    }
    
    @IBAction func setHourToPlus30(sender: AnyObject) {
        self.hourForWS = self.timePlus30.titleLabel?.text;
        println("Hora para WS: \(self.hourForWS)")
        self.timeIni.backgroundColor = self.UIColorFromRGB(0x00FF00);
        self.timePlus30.backgroundColor = self.UIColorFromRGB(0x999999);
        self.timePlus60.backgroundColor = self.UIColorFromRGB(0x00FF00);

    }
    
    @IBAction func setHourToPlus60(sender: AnyObject) {
        self.hourForWS = self.timePlus60.titleLabel?.text;
        println("Hora para WS: \(self.hourForWS)")
        self.timeIni.backgroundColor = self.UIColorFromRGB(0x00FF00);
        self.timePlus30.backgroundColor = self.UIColorFromRGB(0x00FF00);
        self.timePlus60.backgroundColor = self.UIColorFromRGB(0x999999);

    }
    
    @IBAction func continueButtonPressed(sender: AnyObject) {
        //Detectar si existe usuario dentro de la app
        //Manejo de Core Data para ver si en la app hay usuario registrado
        
        
        self.viewContainerForSpinner.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height);
        /****************** INTENTO DE SPINNER *******************/
        self.spinner = UIActivityIndicatorView(frame: CGRectMake(0,0, 100.0, 100.0)) as UIActivityIndicatorView
        self.spinner.center = self.viewContainerForSpinner.center;
        self.spinner.hidesWhenStopped = true;
        self.spinner.color = UIColor(red: 0.0, green: 255.0, blue: 0.0, alpha: 1.0);
        self.spinner.startAnimating();
        self.spinner.autoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
        self.viewContainerForSpinner.backgroundColor = UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.6);
        self.viewContainerForSpinner.addSubview(spinner);
        self.view.addSubview(self.viewContainerForSpinner);
        self.viewContainerForSpinner.layer.zPosition = 2;
        self.view.layer.zPosition = 0;
        /******************** BOTONRES DE FILTROS ****************/

        let appDel:AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate;
        let context = appDel.managedObjectContext!
        let en:NSEntityDescription? = NSEntityDescription.entityForName("User", inManagedObjectContext: context)
        let entName:String! = en?.name;
        let freq = NSFetchRequest(entityName: entName)
        let userList: [NSManagedObject]?  = context.executeFetchRequest(freq, error: nil) as? [NSManagedObject]
        if(userList?.count == 0){
            println("No hay usuario");
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let lwc = storyBoard.instantiateViewControllerWithIdentifier("loginView") as LoginViewController;
            lwc.isFromReservation = true;
            lwc.localSelected = self.localSelected;
            lwc.counter = self.counter;
            lwc.hourForWS =  self.hourForWS;
            lwc.dateForReservation = self.dateSelected;
            lwc.dataOfSelection = self.buttonData;
            self.presentViewController(lwc, animated: true, completion: nil);
            
            
        }else{
            println("Si hay usuario");
            var firstContact = userList![0]
            var pass = firstContact.valueForKey("pass") as String
            var email = firstContact.valueForKey("userEmail") as String;
            var id = firstContact.valueForKey("userId") as Int;
            self.ownerName = firstContact.valueForKey("userName") as String;
            api.callForReservation(self.buttonData, userEmail: email, userPass: pass, idUser: id, idLocal: self.localSelected?.valueForKey("user_id_user") as Int, size:self.counter, hour:self.hourForWS, date: self.dateSelected);
            

        }

        
    }
    
    func didRecieveResponseOfReservation(results :NSDictionary){
        var text = self.buttonData.valueForKey("textToShow") as String;
        self.api.manageReservationResults(results,dateSelected:self.dateSelected, hourForWS: self.hourForWS, localSelected: localSelected!, parentView: self, clientName:self.ownerName, textToShow: text);
        self.spinner.stopAnimating();
        self.viewContainerForSpinner.removeFromSuperview();
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "backToLocalDetail"){
            let viewDetailOfLocal = segue.destinationViewController as DetailLocalViewController;
            viewDetailOfLocal.localSelected = self.localSelected;
            viewDetailOfLocal.counter = self.counter
            viewDetailOfLocal.productSelected = self.productSelected;
            viewDetailOfLocal.dateSelected = self.dateSelected;
            viewDetailOfLocal.tableData = self.tableData;
            viewDetailOfLocal.userLat = self.userLat
            viewDetailOfLocal.userLon = self.self.userLon;
            self.navigationController?.pushViewController(viewDetailOfLocal, animated: false);
        }
        
    }
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }


}
