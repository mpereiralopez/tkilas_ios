//
//  Inscripcion.swift
//  Tkilas
//
//  Created by Miguel Pereira Lopez on 29/09/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit
import CoreData

class Inscripcion: UIViewController, TkilasAPIReservationProtocol,UITextFieldDelegate {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var cpTextField: UITextField!
    var deviceUUID:String?;
    var api: TkilasAPI = TkilasAPI();
    var activeTextField: UITextField!;
    var isFromReservation:Bool!;
    
    //***** Solo si vienes desde reserva *******
    var localSelected :NSDictionary!;
    var counter:Int!;
    var hourForWS:String!;
    var dateForReservation:NSDate!;
    var dataOfSelection:NSDictionary!;
    
    let viewContainerForSpinner: UIView = UIView();
    var spinner: UIActivityIndicatorView!;
    
    var ownerName : String!;
    //******************************************
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad();
        self.deviceUUID = UIDevice.currentDevice().identifierForVendor.UUIDString;
        self.passTextField.secureTextEntry = true;
        self.cpTextField.keyboardType = UIKeyboardType.NumberPad;
        self.emailTextField.keyboardType = UIKeyboardType.EmailAddress;
        
        self.nameTextField.delegate = self;
        self.surnameTextField.delegate = self;

        self.emailTextField.delegate = self;
        self.passTextField.delegate = self;
        self.cityTextField.delegate = self;
        self.cpTextField.delegate = self;

        api.delegateReservation = self;
        //registerForKeyboardNotifications();
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        self.view.endEditing(true);
        return false;
    }
    
    //MARK: - Keyboard Management Methods
    
    // Call this method somewhere in your view controller setup code.
    func registerForKeyboardNotifications() {
        let notificationCenter = NSNotificationCenter.defaultCenter()
        notificationCenter.addObserver(self,
            selector: "keyboardWillBeShown:",
            name: UIKeyboardWillShowNotification,
            object: nil)
        notificationCenter.addObserver(self,
            selector: "keyboardWillBeHidden:",
            name: UIKeyboardWillHideNotification,
            object: nil)
    }
    
    // Called when the UIKeyboardDidShowNotification is sent.
    func keyboardWillBeShown(sender: NSNotification) {
        let info: NSDictionary = sender.userInfo!
        let value: NSValue = info.valueForKey(UIKeyboardFrameBeginUserInfoKey) as NSValue
        let keyboardSize: CGSize = value.CGRectValue().size
        let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your app might not need or want this behavior.
        var aRect: CGRect = self.view.frame
        aRect.size.height -= keyboardSize.height
        let activeTextFieldRect: CGRect? = activeTextField?.frame
        let activeTextFieldOrigin: CGPoint? = activeTextFieldRect?.origin
        if (!CGRectContainsPoint(aRect, activeTextFieldOrigin!)) {
            scrollView.scrollRectToVisible(activeTextFieldRect!, animated:true)
        }
    }
    
    // Called when the UIKeyboardWillHideNotification is sent
    func keyboardWillBeHidden(sender: NSNotification) {
        let contentInsets: UIEdgeInsets = UIEdgeInsetsZero
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func textFieldDidBeginEditing(textField: UITextField!) {
        activeTextField = textField
        scrollView.scrollEnabled = true
    }
    
    func textFieldDidEndEditing(textField: UITextField!) {
        activeTextField = nil
        scrollView.scrollEnabled = false
    }
    

    @IBAction func create_new_user_WS(sender: AnyObject) {
        println("Vamos a crear un usuario")
        
        self.view.endEditing(true);
        self.viewContainerForSpinner.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height);
        /****************** INTENTO DE SPINNER *******************/
        self.spinner = UIActivityIndicatorView(frame: CGRectMake(0,0, 100.0, 100.0)) as UIActivityIndicatorView
        self.spinner.center = self.viewContainerForSpinner.center;
        self.spinner.hidesWhenStopped = true;
        self.spinner.color = UIColor(red: 0.0, green: 255.0, blue: 0.0, alpha: 1.0);
        self.spinner.startAnimating();
        self.spinner.autoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
        self.viewContainerForSpinner.backgroundColor = UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.6);
        self.viewContainerForSpinner.addSubview(spinner);
        self.view.addSubview(self.viewContainerForSpinner);
        self.viewContainerForSpinner.layer.zPosition = 2;
        self.view.layer.zPosition = 0;
        /******************** BOTONRES DE FILTROS ****************/

        
        
        let url:String = API_LOCATION().API_HOST + API_LOCATION().API_CREATE_NEW_MOBILE_USER;
        var request : NSMutableURLRequest = NSMutableURLRequest()
        request.URL = NSURL(string: url)
        println(url)
        request.HTTPMethod = "POST"
        request.addValue(API_LOCATION().API_KEY, forHTTPHeaderField: "API_KEY");
        request.setValue( "application/x-www-form-urlencoded", forHTTPHeaderField: "Content-type")

        
        var bodyData:String = "clientCity="+self.cityTextField.text
        bodyData+="&clientCp="+self.cpTextField.text
        bodyData+="&user_name="+self.nameTextField.text
        bodyData+="&user_surname="+self.surnameTextField.text;
        bodyData+="&email="+self.emailTextField.text;
        bodyData+="&password="+self.passTextField.text;
        bodyData+="&client_SO=1";
        bodyData+="&clientSex=1";
        bodyData+="&deviceUuid="+self.deviceUUID!;
        
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);
        
        println(bodyData);
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            var error: AutoreleasingUnsafeMutablePointer<NSError?> = nil
            let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: error) as? NSDictionary
            
            if (jsonResult != nil) {
                // process jsonResult
                println("OK")
                println(jsonResult);
                let cdu = CoreDataUtils();
                cdu.saveUserToCoreData(jsonResult)
                //Ahora me voy a la vista del perfil
                //CALL TO LOGIN VIEW
                if(self.isFromReservation == true){
                    
                    let appDel:AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate;
                    let context = appDel.managedObjectContext!
                    let en:NSEntityDescription? = NSEntityDescription.entityForName("User", inManagedObjectContext: context)
                    let entName:String! = en?.name;
                    let freq = NSFetchRequest(entityName: entName)
                    let userList: [NSManagedObject]?  = context.executeFetchRequest(freq, error: nil) as? [NSManagedObject]
                    
                    self.ownerName = userList![0].valueForKey("userName") as String;
                    
                    self.api.callForReservation(self.dataOfSelection
                        , userEmail: self.emailTextField.text, userPass: self.passTextField.text, idUser: jsonResult.valueForKey("userIdUser") as Int, idLocal: self.localSelected.valueForKey("user_id_user") as Int, size:self.counter, hour:self.hourForWS, date: self.dateForReservation);
                    
                }else{
                    self.spinner.stopAnimating();
                    self.viewContainerForSpinner.removeFromSuperview();
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                    let pwc = storyBoard.instantiateViewControllerWithIdentifier("profileView") as ProfileViewController;
                    self.presentViewController(pwc, animated: true, completion: nil)
                }
                
            } else {
                // couldn't load JSON, look at error
                println("KO")
                println(error)
            }
            
            
        })
        
        
    }
    
    func didRecieveResponseOfReservation(results :NSDictionary){
        println("didRecieveResponseOfReservation login");
        var text = self.dataOfSelection.valueForKey("textToShow") as String;
        self.api.manageReservationResults(results,dateSelected:self.dateForReservation, hourForWS: self.hourForWS, localSelected: self.localSelected, parentView: self, clientName:self.ownerName, textToShow: text);
        self.spinner.stopAnimating();
        self.viewContainerForSpinner.removeFromSuperview();
    }
    
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        let segueIdentifier:String = segue.identifier!;
        println("#####" + segueIdentifier);
        var viewDetailOfLocal:LoginViewController = segue.destinationViewController as LoginViewController;
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

}
