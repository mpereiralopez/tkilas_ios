//
//  CoreDataUtils.swift
//  Tkilas
//
//  Created by Miguel on 01/10/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataUtils {
    
    
    func saveUserToCoreData(jsonResult:NSDictionary) -> Void{
        println("Dentro de jsonResult")
        
        var appDel:AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate;
        var context:NSManagedObjectContext = appDel.managedObjectContext!;
        var newUser = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: context) as NSManagedObject;
        
        let json = JSONValue(jsonResult)
        
        var userFullName:String = json["user"]["user_name"].string!;
        userFullName += " ";
        userFullName += json["user"]["user_surname"].string!;
        
        newUser.setValue(userFullName, forKey: "userName")
        newUser.setValue(json["userIdUser"].integer, forKey: "userId")
        newUser.setValue(json["user"]["email"].string, forKey: "userEmail")
        newUser.setValue(json["user"]["password"].string, forKey: "pass")
        //newUser.setValue(json["clientProfilePic"].string, forKey: "user_pic");
        
        
        context.save(nil)
        println(newUser)
        println("User saved");
        
    }
    
    func saveOfferToCoreData(jsonResult:NSDictionary) -> Void{
        println("Dentro de saveOfferToCoreData")
        
        var appDel:AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate;
        var context:NSManagedObjectContext = appDel.managedObjectContext!;
        var newOffer = NSEntityDescription.insertNewObjectForEntityForName("Local", inManagedObjectContext: context) as NSManagedObject;
        
        newOffer.setValue(jsonResult.valueForKey("date") as NSDate, forKey: "date");
        newOffer.setValue(jsonResult.valueForKey("hour") as String, forKey: "hour");
        newOffer.setValue(jsonResult.valueForKey("local_name") as String, forKey: "local_name");
        newOffer.setValue(jsonResult.valueForKey("localId") as Int, forKey: "localId");
        newOffer.setValue(jsonResult.valueForKey("offer") as String, forKey: "offer");
        newOffer.setValue(jsonResult.valueForKey("pic_url") as String, forKey: "pic_url");
        newOffer.setValue(jsonResult.valueForKey("size") as Int, forKey: "size");
        //newUser.setValue(userFullName, forKey: "userName")
        //newUser.setValue(json["userIdUser"].integer, forKey: "userId")
        //newUser.setValue(json["user"]["email"].string, forKey: "userEmail")
        //newUser.setValue(json["user"]["password"].string, forKey: "pass")
        //newUser.setValue(json["clientProfilePic"].string, forKey: "user_pic");
        
        
        context.save(nil)
        println(newOffer)
        println("Offer saved");
        
    }
}
