//
//  ProductType.swift
//  Tkilas
//
//  Created by Miguel Pereira Lopez on 17/08/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import Foundation


struct ProductType{

    let BOTTLE:Int = 3;
    let BEER:Int = 0;
    let COFFEE:Int = 1;
    let COCKTAIL:Int = 2;
    let ALL:Int = 4;
    let MOST_POPULAR:Int = 4;
    let BEST_DISCOUNTS: Int = 4;

}


struct ProductSubtype{
    
    let BASIC:Int = 0;
    let PREMIUM:Int = 1;
    let WINE:Int = 2;
    
}