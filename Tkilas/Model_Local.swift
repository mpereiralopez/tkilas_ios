//
//  Model_Local.swift
//  Tkilas
//
//  Created by Miguel on 29/09/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit
import CoreData

class Model_Local: NSManagedObject{

    @NSManaged var date: NSDate;
    @NSManaged var hour: String;
    @NSManaged var local_name:String;
    @NSManaged var localId:Int;
    @NSManaged var offer:String;
    @NSManaged var pic_url:String;
    @NSManaged var size:Int;
    
    

}
