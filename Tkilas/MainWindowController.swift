//
//  ViewController.swift
//  Tkilas
//
//  Created by Miguel Pereira Lopez on 13/08/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit
import CoreData
import Foundation



class MainWindowController: UIViewController, SideMenuDelegate {
   
    var isUser:Bool?;
    var sideMenu : SideMenu?
    var isMenuOpen : Bool = false

    @IBOutlet weak var textLabel: UILabel!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Manejo de Core Data para ver si en la app hay usuario registrado
        let appDel:AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate;
        let context = appDel.managedObjectContext!
        let en:NSEntityDescription? = NSEntityDescription.entityForName("User", inManagedObjectContext: context)
        let entName:String! = en?.name;
        let freq = NSFetchRequest(entityName: entName)
        let userList: [NSManagedObject]?  = context.executeFetchRequest(freq, error: nil) as? [NSManagedObject]
        if(userList?.count == 0){
            println("No hay usuario");
            self.isUser = false;
        }else{
            println("Si hay usuario")
            let firstContact: NSManagedObject = userList![0]
            
            let name = firstContact.valueForKey("userName") as String
            let email = firstContact.valueForKey("userEmail") as String
            
            //AQUI GESTIONO TODO LO DEL USUARIO
            self.isUser = true;

            
        
        }
        
        sideMenu = SideMenu(sourceView: self.view, menuData: ["Mi perfil", "Mis ofertas"])
        sideMenu!.delegate = self
        self.textLabel.layer.zPosition = 0;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sideMenuDidSelectItemAtIndex(index: Int) {
        
        println(index);
        if(index == 0){
            //Tengo que ir al historial
            if(self.isUser == true){
                //CALL TO PROFILE VIEW
                println(self.isUser);
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let pwc = storyBoard.instantiateViewControllerWithIdentifier("profileView") as ProfileViewController;
                self.presentViewController(pwc, animated: true, completion: nil)
            }else{
                //CALL TO LOGIN VIEW
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let lwc = storyBoard.instantiateViewControllerWithIdentifier("loginView") as LoginViewController;
                lwc.isFromReservation = false;
                self.presentViewController(lwc, animated: true, completion: nil)

            }
        }else if (index==1){
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let hwc = storyBoard.instantiateViewControllerWithIdentifier("historialView") as MiHistorialViewController;
            self.presentViewController(hwc, animated: true, completion: nil)
        
        }
    }
        
    @IBAction func leftMenuButtonPressed(sender: AnyObject) {
        println("Has pulsado en el menu lateral");
        sideMenu?.toggleMenu()        
    }
    
    
    @IBAction func beerButton(sender: AnyObject) {
        println("beerButton Pressed");
    }
    
    @IBAction func coffeButton(sender: AnyObject) {
        println("Caffe Pressed");
       
    }
    
    @IBAction func cocktailButton(sender: AnyObject) {
    }
    
    @IBAction func bottleButton(sender: AnyObject) {
    }
    
    @IBAction func allButton(sender: AnyObject) {
    }
    
    @IBAction func bestDiscountsButton(sender: AnyObject) {
    }
    
    @IBAction func mostPopularButton(sender: AnyObject) {
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        println (segue.identifier)
            // pass data to next view
        
        let segueIdentifier:String = segue.identifier!;
        
        var viewControllerDateSelector:DateViewController = segue.destinationViewController as DateViewController;
        
        switch segueIdentifier{
        
        case "bottleSegue":
            viewControllerDateSelector.productSelected = ProductType().BOTTLE;
            
        case "cocktailSegue":
            viewControllerDateSelector.productSelected = ProductType().COCKTAIL;
            
        case "coffeeSegue":
            viewControllerDateSelector.productSelected = ProductType().COFFEE;
            
        case "beerSegue":
            viewControllerDateSelector.productSelected = ProductType().BEER;
            
        case "allSegue":
            viewControllerDateSelector.productSelected = ProductType().ALL;
            
        case "mostPopular":
            viewControllerDateSelector.productSelected = ProductType().ALL;

        case "bestDisscounts":
            viewControllerDateSelector.productSelected = ProductType().ALL;



        
        default:
                println("Este caso no lo contemplo");
        }
    }

}

