//
//  Model_User.swift
//  Tkilas
//
//  Created by Miguel on 29/09/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit
import CoreData


class Model_User: NSManagedObject{
    
    @NSManaged var pass: String;
    @NSManaged var userEmail:String;
    @NSManaged var userId:Int;
    @NSManaged var userName:String;
    @NSManaged var user_pic:NSData;
    
}