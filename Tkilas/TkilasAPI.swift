//
//  TkilasAPI.swift
//  Tkilas
//
//  Created by Miguel on 22/09/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit
import CoreData



protocol TkilasAPIProtocol{
    func didRecieveResponse(results: NSArray);
}

protocol TkilasAPIReservationProtocol{
    func didRecieveResponseOfReservation (results: NSDictionary);
}

class TkilasAPI: NSObject {
    
    var delegate: TkilasAPIProtocol?;
    var delegateReservation:TkilasAPIReservationProtocol?;
    
    func getLocalListAsync(urlPath:String){
        
        let url: NSURL = NSURL(string: urlPath)!
        let session = NSURLSession.sharedSession()
        
        
        
        /*let userPasswordString = username+":"+password
        let userPasswordData = userPasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = userPasswordData!.base64EncodedStringWithOptions(nil)
        let authString = "Basic \(base64EncodedCredential)"*/
        
        //session.configuration.HTTPAdditionalHeaders = ["API_KEY":API_LOCATION().API_KEY, "Authorization":authString];
        session.configuration.HTTPAdditionalHeaders = ["API_KEY":API_LOCATION().API_KEY];
        let task = session.dataTaskWithURL(url, completionHandler: {data, response, error -> Void in
            
            if error != nil {
                // If there is an error in the web request, print it to the console
                println(error.localizedDescription)
            }
            
            var err: NSError?
            var jsonResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as NSArray
            if err != nil {
                // If there is an error parsing JSON, print it to the console
                println("JSON Error \(err!.localizedDescription)")
            }
            
            self.delegate?.didRecieveResponse(jsonResult);

           
        })
        task.resume()        
    }
    
    
    
    func callForReservation(dataOfSelection:NSDictionary, userEmail:String, userPass:String, idUser:Int, idLocal:Int, size:Int, hour:String, date: NSDate){
        
        var urlBase:String = API_LOCATION().API_HOST;
        var bodyData:String = "";
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd";
        let hourFormatter = NSDateFormatter();
        let str = dateFormatter.stringFromDate(date)
        
        //var bodyJSON = {"person": {"name":"Dani","age":"24"}};
        if( dataOfSelection.valueForKey("isDiscount") as Bool == true){
            urlBase = urlBase + API_LOCATION().API_ASOCIATE_DIS;
            
            let obj:[String:AnyObject] = [
                "id":[
                    "id_client":   idUser,
                    "id_local":   idLocal,
                    "date":    str
                ],
                "size":size,
                "hour":"\(str)T\(hour):00.000+0100"
            ]
            
            let jsonString = JSONStringify(obj)
            println(jsonString)
            bodyData = jsonString;
            
            
        }else{
            urlBase = urlBase + API_LOCATION().API_ASOCIATE_PROMO;
            var promo_index = dataOfSelection.valueForKey("promo_index") as Int
            let obj:[String:AnyObject] = [
                "id":[
                    "id_client":   idUser,
                    "id_local":   idLocal,
                    "date":    str
                ],
                "size":size,
                "hour":"\(str)T\(hour):00.000+0100",
                "promo_index":promo_index
            ]
            
            let jsonString = JSONStringify(obj)
            println(jsonString)
            bodyData = jsonString;
            
        }
        
        //let url:String = API_LOCATION().API_HOST + API_LOCATION().API_CLIENT_INFO;
        var request : NSMutableURLRequest = NSMutableURLRequest()
        request.URL = NSURL(string: urlBase)
        println(urlBase)
        request.HTTPMethod = "POST"
        request.addValue(API_LOCATION().API_KEY, forHTTPHeaderField: "API_KEY");
        
        let userPasswordString = userEmail+":"+userPass;
        let userPasswordData = userPasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = userPasswordData!.base64EncodedStringWithOptions(nil)
        let authString = "Basic \(base64EncodedCredential)"
        request.addValue(authString, forHTTPHeaderField: "Authorization");
        request.setValue( "application/json; charset=utf8", forHTTPHeaderField: "Content-type")
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);
        
        var resultsReturn:NSDictionary!;
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, errorWS: NSError!) -> Void in
            var errorSerialization: AutoreleasingUnsafeMutablePointer<NSError?> = nil
            let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error:errorSerialization ) as? NSDictionary
            
            
            if (jsonResult != nil) {
                // process jsonResult
                println("OK  --> callForReservation")
                println(jsonResult);
                self.delegateReservation?.didRecieveResponseOfReservation(jsonResult);

              

                
            } else {
                // couldn't load JSON, look at error
                println("KO")
                println(errorWS)
                println(errorSerialization);
                
                self.delegateReservation?.didRecieveResponseOfReservation(NSDictionary(object: "KO", forKey: "error"));

            }
            
            
        })
        
        

    }
    
    
    func JSONStringify(value: AnyObject, prettyPrinted: Bool = false) -> String {
        var options = prettyPrinted ? NSJSONWritingOptions.PrettyPrinted : nil
        if NSJSONSerialization.isValidJSONObject(value) {
            if let data = NSJSONSerialization.dataWithJSONObject(value, options: options, error: nil) {
                if let string = NSString(data: data, encoding: NSUTF8StringEncoding) {
                    return string
                }
            }
        }
        return ""
    }
    
    func manageReservationResults(results :NSDictionary, dateSelected:NSDate, hourForWS: String, localSelected:NSDictionary, parentView:UIViewController, clientName:String, textToShow:String){
        if results.count>0 {
            if(results.valueForKey("size") as Int > 0){
                println("AQui hay algo");
                
                
                
                
                var dataToSaveCore:NSDictionary = NSDictionary(dictionary: ["date":dateSelected ,
                    "hour":hourForWS ,
                    "local_name":localSelected.valueForKey("local_name") as String,
                    "localId":localSelected.valueForKey("user_id_user") as Int,
                    "offer": textToShow,
                    "pic_url":localSelected.valueForKey("url_pic1") as String,
                    "size":results.valueForKey("size") as Int]);
                
                
                CoreDataUtils().saveOfferToCoreData(dataToSaveCore);
                
                
                let appDel = UIApplication.sharedApplication().delegate as AppDelegate;
                let context = appDel.managedObjectContext!
                let en = NSEntityDescription.entityForName("Local", inManagedObjectContext: context);
                
                let entName:String! = en?.name;
                let freq = NSFetchRequest(entityName: entName)
                var offerListManaged:[NSManagedObject]!  = context.executeFetchRequest(freq, error: nil) as? [NSManagedObject];
                let index = offerListManaged.count;
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                let owc = storyBoard.instantiateViewControllerWithIdentifier("offerView") as OfferSelectedViewController;
                var objAux = offerListManaged[index-1];
                var attr = objAux.entity.attributesByName as NSDictionary
                var info = objAux.dictionaryWithValuesForKeys(attr.allKeys)
                objAux.setValuesForKeysWithDictionary(info);
                owc.jsonResult = info;
                owc.ownerName = clientName;
                owc.offerDetailFromConfirmation = offerListManaged[index-1].valueForKey("offer") as String;
                owc.localSelectedImgURL = offerListManaged[index-1].valueForKey("pic_url") as String;
                owc.localName = offerListManaged[index-1].valueForKey("local_name") as String;
                parentView.presentViewController(owc, animated: true, completion: nil)
                
                
            }
            
            if(results.valueForKey("error") != nil){
                println("ERROR")
            }
        }
    }
}
