//
//  MapViewController.swift
//  Tkilas
//
//  Created by Miguel on 19/09/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit
import MapKit


class MapViewController:  UIViewController,MKMapViewDelegate,UITableViewDelegate, UITableViewDataSource,TkilasAPIProtocol,UIActionSheetDelegate, UIPickerViewDelegate{

    
    var distances = ["1 Km", "2 Km", "5 km", "10 km"];
    
    
    var api: TkilasAPI = TkilasAPI()
    var grayColor:UIColor!
    var localSelectedInTable:NSDictionary!;
    @IBOutlet var tableView: UITableView!
    var tableData: NSArray = NSArray()
    @IBOutlet var theMapView: MKMapView!
    
    @IBOutlet weak var distancePicker: UIPickerView!
    
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    /**************** BOTONRES DE FILTROS *********************/
    @IBOutlet weak var btnFiltroDistancia: UIButton!
    @IBOutlet weak var labelDistance: UILabel!
    
    @IBOutlet weak var btnOrdenacion: UIButton!
    @IBOutlet weak var labelOrdenacion: UILabel!
    
    @IBOutlet weak var btnFiltro: UIButton!
    @IBOutlet weak var labelFiltro: UILabel!
    
    /*********************************************************/
    
    
    /****************** LABELS INFO SOBRE MAPA  **************/
    @IBOutlet weak var num_persons_label: UILabel!
    @IBOutlet weak var hour_label_info: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    /*********************************************************/

    //PARAMETROS RECIBIDOS DE LA ANTERIOR
    var counter:Int!;
    var productSelected: Int!;
    var dateSelected: NSDate!;
    var latitud: String!;
    var longitud: String!;
    
    var hourSelected:String!;
    let viewContainerForSpinner: UIView = UIView();
    var spinner: UIActivityIndicatorView!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(self.tableData.count == 0){
            self.viewContainerForSpinner.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height);
            /****************** INTENTO DE SPINNER *******************/
            self.spinner = UIActivityIndicatorView(frame: CGRectMake(0,0, 100.0, 100.0)) as UIActivityIndicatorView
            self.spinner.center = self.viewContainerForSpinner.center;
            self.spinner.hidesWhenStopped = true;
            self.spinner.color = UIColor(red: 0.0, green: 255.0, blue: 0.0, alpha: 1.0);
            self.spinner.startAnimating();
            self.spinner.autoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
            self.viewContainerForSpinner.backgroundColor = UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.6);
            self.viewContainerForSpinner.addSubview(spinner);
            self.view.addSubview(self.viewContainerForSpinner);
            self.viewContainerForSpinner.layer.zPosition = 2;
            self.theMapView.layer.zPosition = 0;
            self.tableView.layer.zPosition = 0;
            /******************** BOTONRES DE FILTROS ****************/

            
            var urlPath:String = API_LOCATION().API_HOST + API_LOCATION().API_GET_ALL_LOCALS;
            urlPath = urlPath+"?latitud=\(self.latitud)&longitud=\(self.longitud)&specificDate=\(self.manageDatesToShow(self.dateSelected, isHour: false, isFull: true))"
            
            api.getLocalListAsync(urlPath);
            api.delegate = self;
            
            
        }else{
        
            self.navigationBar.topItem?.title = "\(self.tableData.count) resultados";

        }
        
        
        
        self.btnFiltroDistancia.hidden = true;
        self.labelDistance.hidden = true;
        self.btnOrdenacion.hidden = true;
        self.labelOrdenacion.hidden = true;
        self.btnFiltro.hidden = true;
        self.labelFiltro.hidden = true;
        self.distancePicker.hidden = true;
        self.labelDistance.text = self.distances[0];
        /********************************************************/
        
        /****************** LABELS INFO SOBRE MAPA  **************/
        self.num_persons_label.text = String(self.counter);
        self.hour_label_info.text = manageDatesToShow(self.dateSelected, isHour:true, isFull:false);
        self.dateLabel.text = manageDatesToShow(self.dateSelected, isHour:false, isFull:false);
        /*********************************************************/
        
        
        self.grayColor = UIColorFromRGB(0xF7F7F7);
        /****************** COSAS DE MAPAS **********************/
        self.theMapView.zoomEnabled = true;
        self.theMapView.showsUserLocation = true;
        self.theMapView.showsPointsOfInterest = true;
        self.theMapView.showsBuildings = true;
        self.theMapView.userLocationVisible;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
        
        var latDelta: CLLocationDegrees = 0.01;
        var longDelta: CLLocationDegrees = 0.01;
        var theSpan:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta);
        let latAux:NSString = NSString(string:self.latitud);
        let longAux:NSString = NSString(string:self.longitud);
        var mylocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latAux.doubleValue, longAux.doubleValue);
        var theRegion: MKCoordinateRegion = MKCoordinateRegionMake(mylocation, theSpan);
        self.theMapView.setRegion(theRegion, animated: true);
        /****************** COSAS DE MAPAS **********************/

        //self.navigationItem.title = "\(self.tableData.count) resultados";

    }
    
    
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView.image = UIImage(named:"tkilas_GPS.png")
            anView.canShowCallout = true
        }
        else {
            anView.annotation = annotation
        }
        
        return anView
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableData.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("cell") as? LocalListCellTableViewCell
        
        if !(cell != nil) {
            cell = LocalListCellTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }
        
        dispatch_async(dispatch_get_main_queue()) {
            
            if(indexPath.row % 2 == 0){
                cell!.backgroundColor =  UIColor.whiteColor();
            }else{
                cell!.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.03);
                
            }
            //Get our data row
            var rowData: NSDictionary = self.tableData[indexPath.row] as NSDictionary
            //Set the track name
            let localName: String = rowData["local_name"] as String
            let localAddress: String = rowData["address"] as String
            let localDistance = rowData["distance"] as Double
            cell!.localNameLabel.text = localName;
            cell!.localAddressLabel.text = localAddress;
            cell!.localDistance.text = NSString(format: "%.2f Km", localDistance);
            cell!.localImg.layer.zPosition = 0;
            cell!.localDistance.layer.zPosition = 1;

            
            if (rowData["url_pic1"] as? String != nil){
                var urlString = rowData["url_pic1"] as String;
                urlString = API_LOCATION().API_PICS_HOST+urlString;
                println(urlString)
                var url:NSURL = NSURL(string: urlString)!
                var data:NSData = NSData(contentsOfURL: url, options: nil, error: nil)!
                cell!.localImg.image = UIImage(data: data)
            }else{
                cell!.localImg.image = UIImage(named:"local_NOPIC.png")
            }
            
            
            
            /************************** CHECK FOR PACK ***********************************/
            
            if(rowData["pack_date"] as? String != nil && self.isInTime (rowData["time_ini"] as String, time_fin: rowData["time_fin"] as String)){
                cell!.noOffer.text = "";
                if(rowData["discount"] as? Int != nil ){
                    var discountValue = rowData["discount"] as Int;
                    cell!.noOffer.text = "-\(discountValue)% sólo con Tkilas";
                }else{
                    if(rowData["promo_type"] as? Int != nil){
                        //var discountValue = rowData["discount"] as Int;
                        //cell!.noOffer.text = "-\(discountValue)% sólo con Tkilas";
                       cell!.noOffer.text = self.managePromos(rowData["promo_type"] as Int ,
                            subtype: rowData["promo_subtype"] as Int ,
                            size: rowData["promo_size"] as Int ,
                            pvp: rowData["promo_pvp"] as Double ,
                            max_person: rowData["promo_max_person"] as Int );
                    }else{
                        if(rowData["promo_type2"] as? Int != nil){
                            cell!.noOffer.text = self.managePromos(rowData["promo_type2"] as Int ,
                                subtype: rowData["promo_subtype2"] as Int ,
                                size: rowData["promo_size2"] as Int ,
                                pvp: rowData["promo_pvp2"] as Double ,
                                max_person: rowData["promo_max_person2"] as Int );
                        }
                    }
                }
            }
            
            
            /*****************************************************************************/

        
            var auxLat = rowData["local_latitud"] as Double;
            var auxLon = rowData["local_longitud"] as Double;

        
            let latitude: CLLocationDegrees = auxLat;
            let longitud: CLLocationDegrees = auxLon;
            var localLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitud);
            
            
            var annotation = MKPointAnnotation();
            annotation.coordinate = localLocation;
            annotation.title = localName;
            annotation.subtitle = localAddress;
            self.theMapView.addAnnotation(annotation);
        
            cell!.setNeedsLayout();

        }
        //ViewControllerUtils().hideActivityIndicator(tableView);

        return cell!
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.tableView.reloadData()
    }
    
    
    func didRecieveResponse(results :NSArray){
        // var local = new Local
        if results.count>0 {
            self.tableData = results  as NSArray
            self.viewWillAppear(true);
        }
        
   
        
        println("Adios spinner");
        
        self.spinner.stopAnimating();
        self.viewContainerForSpinner.removeFromSuperview();
        //self.navigationItem.title = "\(self.tableData.count) resultados";
        self.navigationBar.topItem?.title = "\(self.tableData.count) resultados";


    }
    
    
    /////// METODOS PARA LOS FILTROS
    @IBAction func btnDistancePressed(sender: AnyObject) {
        self.distancePicker.hidden = false;

    }
    
    // returns the number of 'columns' to display.
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1;
    }
    
    // returns the # of rows in each component..
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return distances.count;
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String!{
        return distances[row]
    
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        self.labelDistance.text = distances[row];
        
        self.distancePicker.hidden = true;
    }
    
    ///////////////////////////////
    
    
    func UIColorFromRGB(rgbValue: UInt) -> UIColor {
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        let segueIdentifier:String = segue.identifier!;
        println("#####" + segueIdentifier);
        
        if(segueIdentifier == "backHomeSiegue"){
            var mainViewController:MainWindowController = segue.destinationViewController as MainWindowController;
        }else{
            
            //let viewDetailOfLocal = DetailLocalViewController(nibName: "DetailLocalViewController", bundle: nil)
            let viewDetailOfLocal:DetailLocalViewController = segue.destinationViewController as DetailLocalViewController;
            var localIndex = self.tableView!.indexPathForSelectedRow()!.row
            self.localSelectedInTable = self.tableData[localIndex] as NSDictionary;
            viewDetailOfLocal.localSelected = self.localSelectedInTable;
            viewDetailOfLocal.counter = self.counter
            viewDetailOfLocal.productSelected = self.productSelected;
            viewDetailOfLocal.dateSelected = self.dateSelected;
            viewDetailOfLocal.tableData = self.tableData;
            viewDetailOfLocal.hourSelected = self.hour_label_info.text;
            viewDetailOfLocal.userLat = self.latitud;
            viewDetailOfLocal.userLon = self.longitud;
            
            
            
            //self.navigationController?.pushViewController(viewDetailOfLocal, animated: false);

        }
        
        
    }
    
    
    func manageDatesToShow(date: NSDate, isHour: Bool, isFull: Bool) -> String{
        var returnValue: String;
    
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.CalendarUnitHour | .CalendarUnitMinute | .DayCalendarUnit | .MonthCalendarUnit | NSCalendarUnit.YearCalendarUnit , fromDate: date)
        var hour = components.hour
        var minutes = components.minute
        var minutesText:String!;
    
        if(minutes<30){
            minutes = 30
        }else{
            if(minutes>30 && minutes < 60){
                minutes = 0;
                hour = hour + 1;
            }
            
        }
        
        if(minutes == 0) {
            minutesText="00";
        }
        if(minutes == 30){
            minutesText="30";
        }
        
        
        if(isFull){
            returnValue = "\(components.year)-\(components.month)-\(components.day)"
        }else{
            (isHour) ? (returnValue = "\(hour):\(minutesText)") : (returnValue = "\(components.day)/\(components.month)");
        }
        
        return returnValue

    
    }
    
    func isInTime(time_ini: String, time_fin: String) ->Bool{
        var boolVal = false;
        let calendar = NSCalendar.currentCalendar();
        let components = calendar.components(NSCalendarUnit.CalendarUnitHour | NSCalendarUnit.CalendarUnitMinute, fromDate: self.dateSelected)
        let hour = components.hour;
        let minutes = components.minute;
        
        let arrayTimeIni:[String] = time_ini.componentsSeparatedByString(":");
        let arrayTimeFin:[String] = time_fin.componentsSeparatedByString(":");
        
        if(hour >= arrayTimeIni[0].toInt() && hour <= arrayTimeFin[0].toInt()){
            println("HERE")
            if(hour == arrayTimeIni[0].toInt()){
                if(minutes >= arrayTimeIni[1].toInt()){
                    boolVal = true;
                }else{
                    boolVal = true;
                }
            }else{
                if(hour == arrayTimeFin[0].toInt()){
                    if(minutes <= arrayTimeFin[1].toInt()){
                        boolVal = true;
                    }else{
                        boolVal = true;
                    }
                }else{
                    boolVal = true;
                }
            
            }
            
           
        }
        
        println("Resultado de isIntime");
        println(boolVal);

        return boolVal;
    }
    
    func managePromos (type: Int, subtype:Int, size:Int, pvp:Double, max_person:Int)->String{
        var returnString: String!;
        let doubleValueSize:Double = Double(size)
        println("Client press \(self.productSelected)");
        println("Local Offer \(type)");
        
        if(self.productSelected == type || self.productSelected == ProductType().ALL){
            switch type {
            case ProductType().BEER:
                (size == 1) ? (returnString = "\(size) cerveza a \(pvp*doubleValueSize) €"):(returnString = "\(size) cervezas a \(pvp*doubleValueSize) €");
                
                break;
                
            case ProductType().COFFEE:
                (size == 1) ? (returnString = "\(size) café a \(pvp*doubleValueSize) €"):(returnString = "\(size) cafés a \(pvp*doubleValueSize) €");

                break;
                
            case ProductType().COCKTAIL:
                (size == 1) ? (returnString = "\(size) copa a \(pvp*doubleValueSize) €") : (returnString = "\(size) copas a \(pvp*doubleValueSize) €");
                break;

                
            case ProductType().BOTTLE:
                (size == 1) ? (returnString = "\(size) botella a \(pvp*doubleValueSize) €") :  (returnString = "\(size) botellas a \(pvp*doubleValueSize) €");

                break;

                
            default:
                //returnString = "\(size) \() a \(pvp*size) €";
                println("BE");
                
            }
        }else{
            returnString = "No existen ofertas para la fecha";
        }
        
        
        
        return returnString;
    }
    
}