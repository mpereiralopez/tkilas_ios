//
//  DetailLocalViewController.swift
//  Tkilas
//
//  Created by Miguel on 23/09/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit
import MapKit


class DetailLocalViewController: UIViewController, MKMapViewDelegate, UIActionSheetDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var navigatonBar: UINavigationBar!
    @IBOutlet weak var localImages: UIImageView!
    @IBOutlet weak var navigationBarTitle: UINavigationItem!
    @IBOutlet weak var contentSubView: UIView!
    @IBOutlet weak var localB1: UIButton!
    @IBOutlet weak var localB2: UIButton!
    @IBOutlet weak var localB3: UIButton!
    @IBOutlet weak var localPriceList: UILabel!
    @IBOutlet weak var localDescription: UILabel!
    @IBOutlet weak var localPayWayIcon: UIImageView!
    @IBOutlet var MapViewLocal: MKMapView!
    @IBOutlet weak var whoToArriveBTN: UIButton!
    @IBOutlet weak var localAddressLabel: UILabel!
    @IBOutlet weak var localPicPageController: UIPageControl!
    var pageImages:[UIImage] = [UIImage]()
    var pageViews:NSMutableArray!;
    
    @IBOutlet weak var noOfferText: UILabel!
    
    let screenSize: CGRect = UIScreen.mainScreen().bounds
    var localLongitud : CLLocationDegrees = 0.0;
    var localLatitud : CLLocationDegrees = 0.0;
    
    var counter: Int!;
    var productSelected: Int!;
    var dateSelected: NSDate!;
    var tableData: NSArray = NSArray();
    var userLat:String!;
    var userLon:String!;
    var pack: Array<AnyObject> = [];
    var button1Dict: NSDictionary!;
    var button2Dict: NSDictionary!;
    var button3Dict: NSDictionary!;
    var hourSelected:String!;

    var localSelected:NSDictionary?;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.scrollView.clearsContextBeforeDrawing = true;
        
        
        var swipeRight = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeRight.direction = UISwipeGestureRecognizerDirection.Right
        self.localPicPageController.addGestureRecognizer(swipeRight)
        
        var swipeLeft = UISwipeGestureRecognizer(target: self, action: "respondToSwipeGesture:")
        swipeLeft.direction = UISwipeGestureRecognizerDirection.Left
        self.localPicPageController.addGestureRecognizer(swipeLeft)
        
        self.localPicPageController.userInteractionEnabled = true;

        //SETTING LOCAL INSIDE JSON OBJECT WRAPPER
        let json = JSONValue(self.localSelected!)
        
         self.localB1.hidden = true;self.localB2.hidden = true;self.localB3.hidden = true;
        
        self.noOfferText.hidden = true;
        self.navigationBarTitle.title = json["local_name"].string
        self.navigatonBar.tintColor = UIColor.whiteColor();

        //SET LOCAL PICTURE
        
        var url1:String = "";
        var url2:String = "";
        var url3:String = "";
        
        if(json["url_pic1"].string != nil){
            url1 = json["url_pic1"].string!
        }
        
        if(json["url_pic2"].string != nil){
            url2 = json["url_pic2"].string!

        }
        
        if(json["url_pic3"].string != nil){
            url3 = json["url_pic3"].string!

        }
        self.checkImagesOfLocal(url1,img2:url2,img3: url3);
        self.localPicPageController.numberOfPages = self.pageImages.count;
        println("Numero de fotos del local: \(self.pageImages.count)");
        self.localPicPageController.currentPage = 0;
        self.localPicPageController.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.view.addSubview(self.localPicPageController);
        self.localImages.image = pageImages[0];
        self.localImages.contentMode = UIViewContentMode.ScaleToFill;
       
        //SET LOCAL ADDRESS
        self.localAddressLabel.text = json["address"].string;
        //SET LOCAL PACK
        
        if(json["pack_date"].string != nil){
            var promoArray: Array<AnyObject> = [];
            if(json["discount"].integer != nil){
                self.pack.append(json["discount"].integer!);
            }
            if(json["promo_type"].integer != nil){
                promoArray.append(json["promo_type"].integer!);
                promoArray.append(json["promo_subtype"].integer!);
                promoArray.append(json["promo_size"].integer!);
                promoArray.append(json["promo_pvp"].double!);
                promoArray.append(json["promo_max_person"].integer!);
                promoArray.append(0);
                self.pack.append(promoArray);
                promoArray = [];
            }
            if(json["promo_type2"].integer != nil){
                promoArray.append(json["promo_type2"].integer!);
                promoArray.append(json["promo_subtype2"].integer!);
                promoArray.append(json["promo_size2"].integer!);
                promoArray.append(json["promo_pvp2"].double!);
                promoArray.append(json["promo_max_person2"].integer!);
                promoArray.append(1);

                self.pack.append(promoArray);
                promoArray = [];

            }
            
            self.setLocalPack(self.pack);
        
        }else{
            self.noOfferText.hidden = false;
        }
        
        
        
        
        //SET LOCAL PRICES
        self.setLocalPrices(json["local_cost_beer"].double, cocktailCost: json["local_cost_cocktail"].double, alchBottleCost: json["local_cost_alch_bottle"].double, wineBottleCost: json["local_cost_wine_bottle"].double, cafeCost: json["local_cost_coffe"].double,tonicCost: json["local_cost_tonic"].double)
        
    
        //SET LOCAL DESCRIPTION
        self.setLocalDescription(json["local_desc"].string)
        
        //SET LOCAL PAYWAY
        self.setLocalPayWay(json["local_pay_way"].integer)
        
        //SET LOCAL IN MAP
        self.setLocalInMap(json["local_latitud"].double, longitud: json["local_longitud"].double, localName: json["local_name"].string, localAdress: json["address"].string)
        
        //SET LOCAL PACKS
        //self.setLocalOffers(json["packs"].array)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //self.scrollView.scrollEnabled = true;
        //self.scrollView.addSubview(self.contentSubView)
        //self.scrollView.contentSize = self.contentSubView.frame.size;
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.scrollView.scrollEnabled = true;
        self.scrollView.addSubview(self.contentSubView)
        self.scrollView.contentSize = self.contentSubView.frame.size;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setLocalPrices( beerCost:Double!, cocktailCost:Double!, alchBottleCost:Double!, wineBottleCost:Double!, cafeCost:Double!, tonicCost:Double!){
        
        var stringToShow = "";
        if(beerCost>0.0) {stringToShow += "Cerveza: \(beerCost.toString()) €\n"}
        if(cocktailCost>0.0) {stringToShow += "Cocktail: \(cocktailCost.toString()) €\n"}
        if(alchBottleCost > 0.0) {stringToShow += "Botella de alcohol: \(alchBottleCost.toString()) €\n"}
        if(wineBottleCost > 0.0) {stringToShow += "Botella de Vino: \(wineBottleCost.toString()) €\n"}
        if(cafeCost>0.0){stringToShow += "Café: \(cafeCost.toString()) €\n"}
        if(tonicCost > 0.0){stringToShow += "Refresco: \(tonicCost.toString())€";}
        self.localPriceList.text = "";
        self.localPriceList.text = stringToShow;
    }
    
    func setLocalDescription (localDescriptionValue:String!){
        self.localDescription.text = localDescriptionValue;
    }
    
    func setLocalPayWay (localPayWayValue:Int!){
        if(localPayWayValue == 0){
            self.localPayWayIcon.image = UIImage(named:"credit_card_NO.png")
        }else{
            self.localPayWayIcon.image = UIImage(named:"credit_card_SI.png")

        }
    }
    
    func setLocalInMap(latitud:Double!, longitud:Double!, localName:String!, localAdress:String!){
        self.localLatitud = latitud;
        self.localLongitud = longitud;
        var latDelta: CLLocationDegrees = 0.01;
        var longDelta: CLLocationDegrees = 0.01;
        var theSpan:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta);
        var localLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(self.localLatitud, self.localLongitud);
        var theRegion: MKCoordinateRegion = MKCoordinateRegionMake(localLocation, theSpan);
        self.MapViewLocal.setRegion(theRegion, animated: true);
        var annotation = MKPointAnnotation();
        annotation.coordinate = localLocation;
        annotation.title = localName;
        annotation.subtitle = localAdress;
        self.MapViewLocal.zoomEnabled = true;
        self.MapViewLocal.addAnnotation(annotation);
        self.MapViewLocal.showsUserLocation = true;
        self.MapViewLocal.showsPointsOfInterest = true;
        self.MapViewLocal.showsBuildings = true;
        self.MapViewLocal.userLocationVisible;
    }
    
    
    func setLocalPack(pack: Array<AnyObject>!){
        var textToShow:String = String();
        var totalPrice:Double;
        switch pack.count{
        case 1:
            println("uno");
            if(pack[0].integerValue != nil){
                textToShow = "-\(pack[0].integerValue)% de descuento sólo con Tkilas";
                self.button2Dict = NSDictionary(objects: [true,pack[0].integerValue,textToShow], forKeys: ["isDiscount","discount","textToShow"])
            }else{
                //es una promo
                switch pack[0][0].integerValue{
                case ProductType().BEER:
                    totalPrice = Double(pack[0][2].integerValue) * pack[0][3].doubleValue;
                    textToShow = "\(pack[0][2].integerValue) cervezas por \(totalPrice) sólo con Tkilas"
                    break;
                    
                case ProductType().COFFEE:
                    totalPrice = Double(pack[0][2].integerValue) * pack[0][3].doubleValue;
                    textToShow = "\(pack[0][2].integerValue) cafés por \(totalPrice) sólo con Tkilas"
                    break;
                    
                case ProductType().COCKTAIL:
                    totalPrice = Double(pack[0][2].integerValue) * pack[0][3].doubleValue;
                    textToShow = "\(pack[0][2].integerValue) copas por \(totalPrice) sólo con Tkilas"
                    break;
                    
                case ProductType().BOTTLE:
                    totalPrice = Double(pack[0][2].integerValue) * pack[0][3].doubleValue;
                    textToShow = "\(pack[0][2].integerValue) botellas por \(totalPrice) sólo con Tkilas"
                    break;
                    
                default:
                    println("No debería de aparecer");
                    break;
                    
                    
                }
                
                self.button2Dict = NSDictionary(objects: [false,pack[0][0].integerValue,pack[0][1].integerValue,textToShow,pack[0][4].integerValue], forKeys: ["isDiscount","type","subtype","textToShow","promo_index"])

            }
            self.localB2.hidden = false;
            self.localB2.setTitle(textToShow, forState: UIControlState.Normal);
        
        
        
        case 2:
            println("dos")
            if(pack[0].integerValue != nil){
                textToShow = "-\(pack[0].integerValue)% de descuento sólo con Tkilas"
                
                self.localB1.hidden = false;
                self.localB1.setTitle(textToShow, forState: UIControlState.Normal);
                self.button1Dict = NSDictionary(objects: [true,pack[0].integerValue,textToShow], forKeys: ["isDiscount","discount","textToShow"]);
                textToShow = "";
                
                switch pack[1][0].integerValue{
                case ProductType().BEER:
                    totalPrice = Double(pack[1][2].integerValue) * pack[1][3].doubleValue;
                    textToShow = "\(pack[1][2].integerValue) cervezas por \(totalPrice) sólo con Tkilas"
                    break;
                    
                case ProductType().COFFEE:
                    totalPrice = Double(pack[1][2].integerValue) * pack[1][3].doubleValue;
                    textToShow = "\(pack[1][2].integerValue) cafés por \(totalPrice) sólo con Tkilas"
                    break;
                    
                case ProductType().COCKTAIL:
                    totalPrice = Double(pack[1][2].integerValue) * pack[1][3].doubleValue;
                    textToShow = "\(pack[1][2].integerValue) copas por \(totalPrice) sólo con Tkilas"
                    break;
                    
                case ProductType().BOTTLE:
                    totalPrice = Double(pack[1][2].integerValue) * pack[1][3].doubleValue;
                    textToShow = "\(pack[1][2].integerValue) botellas por \(totalPrice) sólo con Tkilas"
                    break;
                    
                default:
                    println("No debería de aparecer");
                    break;
                    
                }
                self.button2Dict =  NSDictionary(objects: [false,pack[1][0].integerValue,pack[1][1].integerValue,textToShow,pack[1][4].integerValue], forKeys: ["isDiscount","type","subtype","textToShow","promo_index"])
                self.localB2.hidden = false;
                self.localB2.setTitle(textToShow, forState: UIControlState.Normal );
                
                
            }else{
                //son dos promos
                switch pack[0][0].integerValue{
                case ProductType().BEER:
                    totalPrice = Double(pack[0][2].integerValue) * pack[0][3].doubleValue;
                    textToShow = "\(pack[0][2].integerValue) cervezas por \(totalPrice) sólo con Tkilas"
                    break;
                    
                case ProductType().COFFEE:
                    totalPrice = Double(pack[0][2].integerValue) * pack[0][3].doubleValue;
                    textToShow = "\(pack[0][2].integerValue) cafés por \(totalPrice) sólo con Tkilas"
                    break;
                    
                case ProductType().COCKTAIL:
                    totalPrice = Double(pack[0][2].integerValue) * pack[0][3].doubleValue;
                    textToShow = "\(pack[0][2].integerValue) copas por \(totalPrice) sólo con Tkilas"
                    break;
                    
                case ProductType().BOTTLE:
                    totalPrice = Double(pack[0][2].integerValue) * pack[0][3].doubleValue;
                    textToShow = "\(pack[0][2].integerValue) botellas por \(totalPrice) sólo con Tkilas"
                    break;
                    
                default:
                    println("No debería de aparecer");
                    break;
                }
                
                
                
                self.button1Dict = NSDictionary(objects: [false,pack[0][0].integerValue,pack[0][1].integerValue,textToShow,pack[0][4].integerValue], forKeys: ["isDiscount","type","subtype","textToShow","promo_index"]);
                self.localB1.hidden = false;
                self.localB1.setTitle(textToShow, forState: UIControlState.Normal);
                
                
                switch pack[1][0].integerValue{
                case ProductType().BEER:
                    totalPrice = Double(pack[1][2].integerValue) * pack[1][3].doubleValue;
                    textToShow = "\(pack[1][2].integerValue) cervezas por \(totalPrice) sólo con Tkilas"
                    break;
                    
                case ProductType().COFFEE:
                    totalPrice = Double(pack[1][2].integerValue) * pack[1][3].doubleValue;
                    textToShow = "\(pack[1][2].integerValue) cafés por \(totalPrice) sólo con Tkilas"
                    break;
                    
                case ProductType().COCKTAIL:
                    totalPrice = Double(pack[1][2].integerValue) * pack[1][3].doubleValue;
                    textToShow = "\(pack[1][2].integerValue) copas por \(totalPrice) sólo con Tkilas"
                    break;
                    
                case ProductType().BOTTLE:
                    totalPrice = Double(pack[1][2].integerValue) * pack[1][3].doubleValue;
                    textToShow = "\(pack[1][2].integerValue) botellas por \(totalPrice) sólo con Tkilas"
                    break;
                    
                default:
                    println("No debería de aparecer");
                    break;
                }
                
                self.button2Dict =  NSDictionary(objects: [false,pack[1][0].integerValue,pack[1][1].integerValue,textToShow,pack[1][4].integerValue], forKeys: ["isDiscount","type","subtype","textToShow","promo_index"])
                self.localB2.hidden = false;
                self.localB2.setTitle(textToShow, forState: UIControlState.Normal);

            }

        case 3:
            
            textToShow = "-\(pack[0].integerValue)% de descuento sólo con Tkilas"
            self.localB1.hidden = false;
            self.localB1.setTitle(textToShow, forState: UIControlState.Normal);
            
             self.button1Dict = NSDictionary(objects: [true,pack[0].integerValue,textToShow], forKeys: ["isDiscount","discount","textToShow"]);

            
            
            switch pack[1][0].integerValue{
            case ProductType().BEER:
                totalPrice = Double(pack[1][2].integerValue) * pack[1][3].doubleValue;
                textToShow = "\(pack[1][2].integerValue) cervezas por \(totalPrice) sólo con Tkilas"
                break;
                
            case ProductType().COFFEE:
                totalPrice = Double(pack[1][2].integerValue) * pack[1][3].doubleValue;
                textToShow = "\(pack[1][2].integerValue) cafés por \(totalPrice) sólo con Tkilas"
                break;
                
            case ProductType().COCKTAIL:
                totalPrice = Double(pack[1][2].integerValue) * pack[1][3].doubleValue;
                textToShow = "\(pack[1][2].integerValue) copas por \(totalPrice) sólo con Tkilas"
                break;
                
            case ProductType().BOTTLE:
                totalPrice = Double(pack[1][2].integerValue) * pack[1][3].doubleValue;
                textToShow = "\(pack[1][2].integerValue) botellas por \(totalPrice) sólo con Tkilas"
                break;
                
            default:
                println("No debería de aparecer");
                break;
            }
            
            self.button2Dict =  NSDictionary(objects: [false,pack[1][0].integerValue,pack[1][1].integerValue,textToShow,pack[1][4].integerValue], forKeys: ["isDiscount","type","subtype","textToShow","promo_index"])
            self.localB2.hidden = false;
            self.localB2.setTitle(textToShow, forState: UIControlState.Normal);
            switch pack[2][0].integerValue{
            case ProductType().BEER:
                totalPrice = Double(pack[2][2].integerValue) * pack[2][3].doubleValue;
                textToShow = "\(pack[2][2].integerValue) cervezas por \(totalPrice) sólo con Tkilas"
                break;
                
            case ProductType().COFFEE:
                totalPrice = Double(pack[2][2].integerValue) * pack[2][3].doubleValue;
                textToShow = "\(pack[2][2].integerValue) cafés por \(totalPrice) sólo con Tkilas"
                break;
                
            case ProductType().COCKTAIL:
                totalPrice = Double(pack[2][2].integerValue) * pack[2][3].doubleValue;
                textToShow = "\(pack[2][2].integerValue) copas por \(totalPrice) sólo con Tkilas"
                break;
                
            case ProductType().BOTTLE:
                totalPrice = Double(pack[2][2].integerValue) * pack[2][3].doubleValue;
                textToShow = "\(pack[2][2].integerValue) botellas por \(totalPrice) sólo con Tkilas"
                break;
                
            default:
                println("No debería de aparecer");
                break;
            }
            
            self.button3Dict =  NSDictionary(objects: [false,pack[2][0].integerValue,pack[2][1].integerValue,textToShow,pack[2][4].integerValue], forKeys: ["isDiscount","type","subtype","textToShow","promo_index"])
            self.localB3.hidden = false;
            self.localB3.setTitle(textToShow, forState: UIControlState.Normal);
            
        default:
            println("Vacio");
            self.noOfferText.hidden = false;
        }
    }
    
    
    
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if !(annotation is MKPointAnnotation) {
            return nil
        }
        
        let reuseId = "LocalMap"
        
        var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView.image = UIImage(named:"tkilas_GPS.png")
            anView.canShowCallout = true
        }
        else {
            anView.annotation = annotation
        }
        
        return anView
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        println(segue.identifier)
        
        
        
        
        if(segue.identifier == "backToMap"){
            
            let viewMap = segue.destinationViewController as MapViewController;
            viewMap.counter = self.counter
            viewMap.productSelected = self.productSelected;
            viewMap.dateSelected = self.dateSelected;
            viewMap.latitud = self.userLat;
            viewMap.longitud = self.userLon;
            viewMap.tableData = self.tableData;
            viewMap.hourSelected = self.hourSelected;
            self.navigationController?.popToViewController(viewMap, animated: true);
            
        }else{
            
            
            
            
            let confirm = segue.destinationViewController as ConfirmViewController;
            confirm.counter = self.counter
            confirm.productSelected = self.productSelected;
            confirm.dateSelected = self.dateSelected;
            confirm.userLat = self.userLat;
            confirm.userLon = self.userLon;
            confirm.tableData = self.tableData;
            confirm.localSelected = self.localSelected;
            confirm.hourSelected = self.hourSelected
            if(segue.identifier == "detailB1Press"){
                confirm.buttonData = self.button1Dict;
            }
            
            if(segue.identifier == "detailB2Press"){
                confirm.buttonData = self.button2Dict;

            }
            
            if(segue.identifier == "detailB3Press"){
                confirm.buttonData = self.button3Dict;

            }
        
            self.navigationController?.popToViewController(confirm, animated: true);

        
        }
    }
    
    @IBAction func goToAppleMapsForGPSRoute(sender: AnyObject) {
        var  actionSheet:UIActionSheet;
        if(UIApplication.sharedApplication().canOpenURL(NSURL(string:"congooglemaps://")!)){
            actionSheet = UIActionSheet(title: "Abrir navegador GPS", delegate: self, cancelButtonTitle: "Cancelar", destructiveButtonTitle: nil, otherButtonTitles: "Abrir en Apple Maps", "Abrir en Google Maps")
        }else{
            actionSheet = UIActionSheet(title: "Abrir navegador GPS", delegate: self, cancelButtonTitle: "Cancelar", destructiveButtonTitle: nil, otherButtonTitles: "Abrir en Apple Maps")
        }
        
        actionSheet.showInView(self.view)
    }
    
    
    
    func actionSheet(actionSheet: UIActionSheet!, clickedButtonAtIndex buttonIndex: Int)
    {
        switch buttonIndex{
            
        case 0:
            NSLog("Cancelar");
            break;
        case 1:
            NSLog("AppleMaps");
            var coordinateLocal:CLLocationCoordinate2D  = CLLocationCoordinate2DMake(self.localLatitud, self.localLongitud);
            var endLocation: MKPlacemark = MKPlacemark(coordinate: coordinateLocal, addressDictionary: nil)
            var launchOptions=[ MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving]
            var endingItem:MKMapItem = MKMapItem (placemark: endLocation);
            endingItem.openInMapsWithLaunchOptions(launchOptions)
            break;
        case 2:
            NSLog("GoogleMaps");
            var stringURLTOMapsGoogle = "congooglemaps://?daddr= \(self.localLatitud),\(self.localLongitud)&directionmode=walking";
            UIApplication.sharedApplication().openURL(NSURL(string: stringURLTOMapsGoogle)!);
            break;
        default:
            NSLog("Default");
            break;
            //Some code here..
            
        }
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
    
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
    
            switch swipeGesture.direction {
                case UISwipeGestureRecognizerDirection.Right:
                    println("Swiped right")
                    showPrevPic();

                case UISwipeGestureRecognizerDirection.Left:
                    println("Swiped left")
                    showNextPic();

                default:
                break
            }
        }
    }
    
    func showNextPic(){
        var actualPic = localPicPageController.currentPage;
        println(actualPic)
        
        if((actualPic + 1) < self.localPicPageController.numberOfPages){
            UIView.transitionWithView(self.localImages, duration: 1.0, options: UIViewAnimationOptions.TransitionFlipFromLeft, animations: { () -> Void in
                self.localImages.image = self.pageImages[actualPic+1];
            }, completion: nil)
            
            self.localPicPageController.currentPage = actualPic+1;

            
        }
   
    }
    
    
    func showPrevPic(){
        var actualPic = localPicPageController.currentPage;
        if((actualPic - 1) >= 0){
            UIView.transitionWithView(self.localImages, duration: 1.0, options: UIViewAnimationOptions.TransitionFlipFromRight, animations: { () -> Void in
                self.localImages.image = self.pageImages[actualPic-1];
                }, completion: nil)
            
            self.localPicPageController.currentPage = actualPic-1;
        }
    
    }
    
    func checkImagesOfLocal(img1: String?, img2: String?, img3: String?){
        var urlString: String;
        var url:NSURL;
        var data:NSData;
        
        if(img1? != ""){
            urlString = API_LOCATION().API_PICS_HOST+img1!;
            url = NSURL(string: urlString)!
            data = NSData(contentsOfURL: url, options: nil, error: nil)!
            self.pageImages.append(UIImage(data: data)!)
        }
        
        if(img2? != ""){
            urlString = API_LOCATION().API_PICS_HOST+img2!;
            url = NSURL(string: urlString)!
            data = NSData(contentsOfURL: url, options: nil, error: nil)!
            self.pageImages.append(UIImage(data: data)!)
        }
        
        if(img3? != ""){
            urlString = API_LOCATION().API_PICS_HOST+img3!;
            url = NSURL(string: urlString)!
            data = NSData(contentsOfURL: url, options: nil, error: nil)!
            self.pageImages.append(UIImage(data: data)!)
        }
    }
}



extension Double {
    func toString() -> String {
        return String(format: "%.1f",self)
    }
}
