//
//  LoginViewController.swift
//  Tkilas
//
//  Created by Miguel Pereira Lopez on 29/09/14.
//  Copyright (c) 2014 Miguel Pereira Lopez. All rights reserved.
//

import UIKit
import CoreData

class LoginViewController: UIViewController, TkilasAPIReservationProtocol,UITextFieldDelegate{

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    
    var api: TkilasAPI = TkilasAPI();

    var isFromReservation:Bool!;
    
    //***** Solo si vienes desde reserva *******
    var localSelected :NSDictionary!;
    var counter:Int!;
    var hourForWS:String!;
    var dateForReservation:NSDate!;
    var dataOfSelection:NSDictionary!;
    
    let viewContainerForSpinner: UIView = UIView();
    var spinner: UIActivityIndicatorView!;

    var ownerName : String!;
    //******************************************
    override func viewDidLoad() {
        super.viewDidLoad()
        println("Dentro de LoginViewController")
        self.passTextField.secureTextEntry = true;
        self.emailTextField.keyboardType = UIKeyboardType.EmailAddress;
        
        self.passTextField.delegate = self;
        self.emailTextField.delegate = self;

        
        api.delegateReservation = self;
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        self.view.endEditing(true);
        return false;
    }

    @IBAction func logMeIn(sender: AnyObject) {
        println("Vamos a llamar al WS");
        self.emailTextField.endEditing(true);
        self.passTextField.endEditing(true);
        self.view.endEditing(true);
        self.viewContainerForSpinner.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height);
        /****************** INTENTO DE SPINNER *******************/
        self.spinner = UIActivityIndicatorView(frame: CGRectMake(0,0, 100.0, 100.0)) as UIActivityIndicatorView
        self.spinner.center = self.viewContainerForSpinner.center;
        self.spinner.hidesWhenStopped = true;
        self.spinner.color = UIColor(red: 0.0, green: 255.0, blue: 0.0, alpha: 1.0);
        self.spinner.startAnimating();
        self.spinner.autoresizingMask = UIViewAutoresizing.FlexibleHeight | UIViewAutoresizing.FlexibleWidth;
        self.viewContainerForSpinner.backgroundColor = UIColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.6);
        self.viewContainerForSpinner.addSubview(spinner);
        self.view.addSubview(self.viewContainerForSpinner);
        self.viewContainerForSpinner.layer.zPosition = 2;
        self.view.layer.zPosition = 0;
        /******************** BOTONRES DE FILTROS ****************/

        
        
        let url:String = API_LOCATION().API_HOST + API_LOCATION().API_CLIENT_INFO;
        var request : NSMutableURLRequest = NSMutableURLRequest()
        request.URL = NSURL(string: url)
        println(url)
        request.HTTPMethod = "GET"
        request.addValue(API_LOCATION().API_KEY, forHTTPHeaderField: "API_KEY");
        
        let userPasswordString = self.emailTextField.text+":"+self.passTextField.text
        let userPasswordData = userPasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = userPasswordData!.base64EncodedStringWithOptions(nil)
        let authString = "Basic \(base64EncodedCredential)"
        
        request.addValue(authString, forHTTPHeaderField: "Authorization");


        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue(), completionHandler:{ (response:NSURLResponse!, data: NSData!, errorWS: NSError!) -> Void in
            var errorSerialization: AutoreleasingUnsafeMutablePointer<NSError?> = nil
            let jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error:errorSerialization ) as? NSDictionary
                
            
            if (jsonResult != nil) {
                // process jsonResult
                println("OK --> logMeIn")
                println(jsonResult);
                let cdu = CoreDataUtils();
                cdu.saveUserToCoreData(jsonResult);
                //Ahora me voy a la vista del perfil
                //CALL TO LOGIN VIEW
                    //Code that presents or dismisses a view controller here
                    if(self.isFromReservation == true){
                        
                        let appDel:AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate;
                        let context = appDel.managedObjectContext!
                        let en:NSEntityDescription? = NSEntityDescription.entityForName("User", inManagedObjectContext: context)
                        let entName:String! = en?.name;
                        let freq = NSFetchRequest(entityName: entName)
                        let userList: [NSManagedObject]?  = context.executeFetchRequest(freq, error: nil) as? [NSManagedObject]

                        self.ownerName = userList![0].valueForKey("userName") as String;
                        
                        self.api.callForReservation(self.dataOfSelection
                            , userEmail: self.emailTextField.text, userPass: self.passTextField.text, idUser: jsonResult.valueForKey("userIdUser") as Int, idLocal: self.localSelected.valueForKey("user_id_user") as Int, size:self.counter, hour:self.hourForWS, date: self.dateForReservation);
                        
                    }else{
                        self.spinner.stopAnimating();
                        self.viewContainerForSpinner.removeFromSuperview();

                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let pwc = storyBoard.instantiateViewControllerWithIdentifier("profileView") as ProfileViewController;
                        self.presentViewController(pwc, animated: true, completion: nil)
                    }
                    
                
            } else {
                // couldn't load JSON, look at error
                println("KO")
                println(errorWS)
                println(errorSerialization)
            }
            
            
        })
        
        

        

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation*/
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        println (segue.identifier)
        // pass data to next view
        
        if(segue.identifier == "singinSegue"){
            var ins:Inscripcion = segue.destinationViewController as Inscripcion;
            ins.isFromReservation = self.isFromReservation;
            ins.localSelected = self.localSelected;
            ins.counter = self.counter;
            ins.hourForWS =  self.hourForWS;
            ins.dateForReservation = self.dateForReservation;
            ins.dataOfSelection = self.dataOfSelection;

        }
        
        if(segue.identifier == "back_to_login"){
             var viewControllerDateSelector:MainWindowController = segue.destinationViewController as MainWindowController;
        }
        
        
        
      

    }
    
    func didRecieveResponseOfReservation(results :NSDictionary){
        println("didRecieveResponseOfReservation login");
        var text = self.dataOfSelection.valueForKey("textToShow") as String;
        self.api.manageReservationResults(results,dateSelected:self.dateForReservation, hourForWS: self.hourForWS, localSelected: self.localSelected, parentView: self, clientName:self.ownerName, textToShow: text);
        self.spinner.stopAnimating();
        self.viewContainerForSpinner.removeFromSuperview();
    }


}
